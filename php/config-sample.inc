<?php
// for older systems this file may be located in various folders and may be named config.inc, database.inc or .database.inc

// set up database vars
$DSDATABASE = "guarisco2018";
$DSHOST="localhost";
$DSUSER="root";
$DSPASSWORD="PASSWORD";
$DSTITLE="Desktop Solutions";

$DSPRODUCTTHUMBURL="http://lamp-localhost/guariscogallery/public_html/catalogthumbs";
$DSPRODUCTTHUMBDIR="/var/www/guariscogallery/public_html/catalogthumbs";

$DSUPLOADURL="http://lamp-localhost/guariscogallery/public_html/catalogimages";
$DSUPLOADDIR="/var/www/guariscogallery/public_html/catalogimages";
