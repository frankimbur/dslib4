<?php

class of_file extends of_element
{

    var $isfile = true;
    var $size;
	var $displaypiceditlink = FALSE;
	var $upload_dir;
	var $upload_url;

    function __construct($a)
    {
        $this->setup_element($a);
    }

    function self_get_frozen($val, $which, &$count)
    {
        return "<input type='input' name='$this->name' size='40' disabled value='$this->value' class='$this->classname'>";
    }


    function self_get($val=NULL,$which=NULL, &$count=NULL)
    {
        #if ($this->displaypiceditlink) {
        	$this->classname='form-control-inline df_file_button' ;
        #}
    	$required = ($this->minlength || $this->required) && ! strlen($this->value) ? "required" : "";
        $str = "";
        $str .= "<input type='hidden' name='MAX_FILE_SIZE' value=$this->size>\n";
        $str .= "<input type='file' value='$this->value' $required class='$this->classname' name='$this->name'";
        if ($this->extrahtml)
            $str .= " $this->extrahtml";
        $str .= ">";
        return $str;
    }

}
