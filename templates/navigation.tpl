{* navigation *}
<nav class="navbar-default navbar-static-side" role="navigation" >
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li>
                <a href=""><i class="fa fa-th-large"></i> <span class="nav-label">Maintain</span> <span
                            class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <LI><a href="safes_browse.php">Safes</a></LI>
                    <LI><a href="auth_user_browse.php">Safe Maintenance Users</a></LI>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-th-large"></i> <span class="nav-label">Utilities</span> <span
                            class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <LI><a href="http://www.ssarmored.com/system/login.php">SS Armored System</a></LI>
                    <LI><a href="http://api.e2c.io/Enterprise/System">E2C System</a></LI>
                </ul>
            </li>
        </ul>

    </div>
</nav>
