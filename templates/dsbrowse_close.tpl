{* last thing before body close *}
{literal}
<script>
    $(document).ready(function(){
        $('.dataTable-{/literal}{$classname}{literal}').DataTable({
            columnDefs: [ {
                orderable: false,
                render: function ( data, type, row, meta ) {
                    return '{/literal}{$editbuttonjs}{literal}'+'&nbsp'+'{/literal}{$deletebuttonjs}{literal}'+'{/literal}{$viewbuttonjs}{literal}';},
                targets:   {/literal}{$hidefrom}{literal},
                class: "text-center text-nowrap"
                /*visible: false, targets: -1 this makes a column invisible*/
            } ],
            order: [[ {/literal}{$orderby}, "{$order}"{literal} ]],
            pageLength: {/literal}{$displayrows}{literal},
            responsive: true,
            dom: '<"html5buttons"B>lgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: '{/literal}{$classname}{literal}'},
                {extend: 'pdf', title: '{/literal}{$classname}{literal}'},
                /* todo: hide the button column from excel csv & print */
                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ],
            search:false,
            processing: true,
            serverSide: true,
            ajax: "{/literal}{$scriptname}{literal}"
        });

    });

</script>


{/literal}
