<?php
/**
* PHP 5.4 Compatibility Functions
* Include at the beginning of each program to restore functionality removed in PHP 5.4
*
* NEVER MAKE PROJECT-SPECIFIC CHANGES TO THIS FILE!!!
* ANY CHANGES SHOULD BE REFLECTED IN THE WIKI.
*
* @version rev-003
* @date 2014 October 31
* @link http://lserv2.dtopinc.com/mediawiki/index.php/PHP_5.4_Compatibility
*/

/* Forces all GET and POST globals to register and be magically quoted.
* This forced register_globals and magic_quotes_gpc both act as if
* they were turned ON even if turned off in your php.ini file.
*
* Reason behind forcing register_globals and magic_quotes is for legacy
* PHP scripts that need to run with PHP 5.4 and higher.  PHP 5.4+ no longer
* support register_globals and magic_quotes, which breaks legacy PHP code.
*
* This is used as a workaround, while you upgrade your PHP code, yet still
* allows you to run in a PHP 5.4+ environment.
*
* Licenced under the GPLv2. Matt Kukowski Sept. 2013
* @added rev-001
*/



/**
* Restore long server arrays
* @added rev-001
* Moved UNDER the magic_quotes fix in rev-003 so these variables get set with the "fixed"
* versions of $_POST, etc.
*/
session_start();
$HTTP_SERVER_VARS =    $_SERVER;
$HTTP_GET_VARS      =    $_GET;
$HTTP_POST_VARS      = $_POST;
$HTTP_POST_FILES  =    $_FILES;
$HTTP_SESSION_VARS=    $_SESSION;
$HTTP_ENV_VARS      =    $_ENV;
$HTTP_COOKIE_VARS =    $_COOKIE;



/**
* function to emulate the register_globals setting in PHP
* for all of those diehard fans of possibly harmful PHP settings :-)
* @author Ruquay K Calloway
* @added rev-001
* @param string $order order in which to register the globals, e.g. 'egpcs' for default
* @link http://www.php.net/manual/en/security.globals.php#82213
*/
function register_globals($order = 'egpcs')
{
    // define a subroutine
    if(!function_exists('register_global_array'))
    {
        function register_global_array(array $superglobal)
        {
            foreach($superglobal as $varname => $value)
            {
                global $$varname;
                $$varname = $value;
            }
        }
    }

    $order = explode("\r\n", trim(chunk_split($order, 1)));
    foreach($order as $k)
    {
        switch(strtolower($k))
        {
            case 'e':    register_global_array($_ENV);        break;
            case 'g':    register_global_array($_GET);        break;
            case 'p':    register_global_array($_POST);        break;
            case 'c':    register_global_array($_COOKIE);    break;
            case 's':    register_global_array($_SERVER);    break;
        }
    }
}

/**
* Undo register_globals
* @author Ruquay K Calloway
* @added rev-001
* @link http://www.php.net/manual/en/security.globals.php#82213
*/
function unregister_globals() {
    if (ini_get(register_globals)) {
        $array = array('_REQUEST', '_SESSION', '_SERVER', '_ENV', '_FILES');
        foreach ($array as $value) {
            foreach ($GLOBALS[$value] as $key => $var) {
                if ($var === $GLOBALS[$key]) {
                    unset($GLOBALS[$key]);
                }
            }
        }
    }
}


/**
* PHP 5.4.x Session Compatibility
* Restores session_register(), session_is_registered() and session_unregister()
* @author Frank Imburgio
* @added rev-002
* @link http://lserv2.dtopinc.com/mediawiki/index.php/PHP_5.4_Compatibility
*/
if ( ! function_exists ( 'session_register' ) ) {
    function session_register(){
        $args = func_get_args();
        foreach ($args as $key){
            $_SESSION[$key]=$GLOBALS[$key];
        }
    }
    function session_is_registered($key){
        if (empty($_SESSION[$key]))
            return false;
        else
            return true;
    }
    function
    session_unregister($key){
        unset($_SESSION[$key]);
    }
}


//Auto init
register_globals();

if (! isset($PXM_REG_GLOB)) {

    $PXM_REG_GLOB = 1;

    if (! ini_get('register_globals')) {
        foreach (array_merge($_GET, $_POST) as $keyy => $vall) {
            global $$keyy;
            if (! is_array($vall)) {
                $$keyy = (get_magic_quotes_gpc()) ? $vall : addslashes($vall);
            }
        }
    }
    if (! get_magic_quotes_gpc()) {
        foreach ($_POST as $keyy => $vall) {
            $_POST[$keyy] = @addslashes($vall);
        }
        foreach ($_GET as $keyy => $vall)  {
            $_GET[$keyy]  = @addslashes($vall);
        }
    }
}
