<?php
########################################################################
# Copyright 1999 Desktop Solutions Software, Inc.
# 516-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
#$Id: auth_user_browse.dtop,v 1.2 2001/11/29 21:18:52 frank Exp $
require( "php/prepend.php" );
page_open( array(
	"sess" => "DS_Session",
	"auth" => "DS_Auth",
	"perm" => "DS_Perm"
) );
$t                = new DSBrowse;
$db               = new DB_Example;
$t->TableWidth    = $DSTABLEWIDTH;
$t->TopHeading    = "Safe Maint Users";
$t->Key           = "uid"; //must match case used in select statement!
$t->classname     = "auth_user";
$t->db            = $db;
$t->Columns       = array("username","last","first","uid");
$t->fields        = array( "Email ID", "Last", "First" );
$t->HideFrom      = count( $t->fields );
$t->SearchChoices = array( "Email ID" => "username", "Last" => "last");

## Optional settings:
$UserRecord     = GetUsersRecord();
$t->ChangeOrder = 1;
DSBeginPage( 1 );
$t->display();
DSEndPage();
?>
