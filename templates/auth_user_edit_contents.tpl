<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>User Detail</h5>
            </div>
            <div class="ibox-content">
                <form method="post" enctype="multipart/form-data" class="form-horizontal">
                    {if $delete}
                        <div class="form-group">
                            <div class="alert alert-danger">Press SUBMIT to delete this record...</div>
                        </div>
                    {/if}

                    {if $delete}
                    <fieldset disabled="disabled">
                        {/if}
                        <input type=hidden name=key value={$key}>
                        <input type="hidden" name="uid" value="{$uid}"/>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Email ID:<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="field_username" size=40 value="{$CurrentRecord->username}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    First Name:<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" required name="field_first" size=40 value="{$CurrentRecord->first}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Last Name:<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" required name="field_last" size=40 value="{$CurrentRecord->last}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Password:<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" required name="field_password" size=40 value="{$password}"/>
                            </div>
                        </div>

                        {if $delete}
                    </fieldset>
                    {/if}
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-primary" type="submit" name="Submit" value="Submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
