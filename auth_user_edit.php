<?php
require( "php/prepend.php" );
page_open( array(
	"sess" => "DS_Session",
	"auth" => "DS_Auth",
	"perm" => "DS_Perm"
) );
page_close();
$form = new DSForm;
global $key;
$CurrentRecord = ReadCurrentRecord( "auth_user", "uid", $key );
if ( isset( $do_over ) ) {
	// we've been called from ourselves, second time in a row...
	// the key should have been passed in, causing defaults to be loaded above
	// now when we unset , the rest of the script will be in 'add' mode..
	unset( $key );
}
//Set up fields here
if ( isset( $key ) ) {
	$form->add_element( array(
		"type"  => "hidden",
		"name"  => "key",
		"value" => $key
	) );
}
if ( isset( $delete ) ) {
	$form->add_element( array(
		"type"  => "hidden",
		"name"  => "delete",
		"value" => 1
	) );
}

$pwtext = OneSQLValue( "
         select decode(password,'post') as pw
         from auth_user where uid='$key'" );

if ( $Submit ) {
	$HTTP_POST_VARS["field_password"] = addslashes( OneSQLValue( "SELECT Encode('$field_password','post')" ) );
	// send to SQL
	if (  $key  ) {
		// UPDATE
		if ( ! isset( $delete ) ) {
			$retval = DoUpdate( $HTTP_POST_VARS, "auth_user", "uid", $key );
		} else {
			// DELETE
			if ( strlen( $RICheck ) ) {
			} else {
				$retval = DoDelete( "auth_user", "uid", $key );
			}
		}
	} else {
		$retval = DoInsert( $HTTP_POST_VARS, "auth_user" );
	}
	if ( $retval ) {
		$form->SaveAndReturn();
	}

	return;
}


DSBeginPage("User Record");
$smarty->assign( 'key', $key );
$smarty->assign( 'password', $pwtext );
$smarty->assign( 'CurrentRecord', $CurrentRecord );
DSEndPage("auth_user_edit.tpl");
#$smarty->display( 'auth_user_edit.tpl' );
?>
