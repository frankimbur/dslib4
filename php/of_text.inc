<?php
/* OOHForms: text
 *
 * Copyright (c) 1998 by Jay Bloodworth
 *
 * $Id: of_text.inc,v 1.2 2001/11/30 16:17:32 frank Exp $
 */

class of_text extends of_element {

  var $maxlength;
  var $minlength;
  var $length_e;
  var $valid_regex;
  var $valid_icase;
  var $valid_e;
  var $pass;
  var $size;
  var $date;
  var $email;

  // Constructor
  function __construct($a) {
    $this->setup_element($a);
    if ($a["type"]=="password")
      $this->pass=1;
  }

  function self_get($val=NULL,$which=NULL, &$count=NULL) {
    $str = "";
    
    if (is_array($this->value))
      $v = htmlspecialchars($this->value[$which]);
    else 
      $v = htmlspecialchars($this->value);
    $n = $this->name . ($this->multiple ? "[]" : "");
    $str .= "<input name='$n' value=\"$v\" id='$n'";
    $type='text';
    if ($this->pass) {
      $type='password';
    }
    if ($this->date) {
        $type='date';
    }
      if ($this->email) {
          $type='email';
      }


    $required=$this->minlength || $this->required ? "required" : "";
    $str .= " type='$type' class='$this->classname' $required";
    if ($this->maxlength)
      $str .= " maxlength='$this->maxlength'";
    if ($this->size) 
      $str .= " size='$this->size'";
    if ($this->extrahtml) 
      $str .= " $this->extrahtml";
    $str .= " />";
    
    $count = 1;
    return $str;
  }

  function self_get_frozen($val,$which, &$count) {
    $str = "";
    
    if (is_array($this->value))
      $v = $this->value[$which];
    else 
      $v = $this->value;
    $n = $this->name . ($this->multiple ? "[]" : "");
    $str .= "<input size='$this->size' type='text' name='$n' value='$v' class='$this->classname' disabled>";
    #$str .= "$v";

    $count = 1;
    return $str;
  }

  function self_get_js($ndx_array) {
    $str = "";
    
    @reset($ndx_array);
    while (list($k,$n) = @each($ndx_array)) {
        
      if ($this->length_e) {
        $str .= "if (f.elements['$this->name'].value.length < $this->minlength) {\n";
        $str .= "  alert(\"$this->length_e\");\n";
        $str .= "  f.elements['$this->name'].focus();\n";
        $str .= "  return(false);\n}\n";
      }
      if ($this->valid_e) {
        $flags = ($this->icase ? "gi" : "g");
        $str .= "if (window.RegExp) {\n";
        $str .= "  var reg = new RegExp(\"$this->valid_regex\",\"$flags\");\n";
        $str .= "  if (!reg.test(f.elements['$this->name'].value)) {\n";
        $str .= "    alert(\"$this->valid_e\");\n";
        $str .= "    f.elements['$this->name'].focus();\n";
        $str .= "    return(false);\n";
        $str .= "  }\n}\n";
      }
    }
    
    return $str;
  }

  function self_validate($val) {
    if (!is_array($val)) $val = array($val);
    reset($val);
    while (list($k,$v) = each($val)) {
      if ($this->length_e && (strlen($v) < $this->minlength))
        return $this->length_e;
      if ($this->valid_e && (($this->icase && 
            !eregi($this->valid_regex,$v)) ||
           (!$this->icase &&
            !ereg($this->valid_regex,$v))))
        return $this->valid_e;
    }
    return false;
  } 

} // end TEXT


