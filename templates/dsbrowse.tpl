<!DOCTYPE html>
<html>

{include file="open.tpl"}

<body>

<div id="wrapper">
    {include  file="navigation.tpl"}

    <div id="page-wrapper" class="gray-bg">
        {include file="top_menu.tpl"}
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left m-t-lg">
                        {include file="dsbrowse_contents.tpl"}
                    </div>
                </div>
            </div>
        </div>

        {include file="footer.tpl"}

    </div>
</div>

{include file="dsbrowse_close.tpl"}

</body>

</html>
