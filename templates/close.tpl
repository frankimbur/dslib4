{* last thing before body close *}
{literal}
    <script>jQuery('a.popup').colorbox({width:"90%",height: "90%",iframe:true});</script>
    <script>
        $('#summernote').summernote({
            height:200,
            width:800,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font'],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
            ]
        });
    </script>


{/literal}