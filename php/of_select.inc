<?php

class of_select extends of_element
{

    var $options;
    var $size;
    var $valid_e;

    // Constructor
    function __construct($a)
    {
        $this->setup_element($a);
        if ($a["type"] == "select multiple") $this->multiple = 1;
    }

    function self_get($val=NULL,$which=NULL, &$count=NULL)
    {
        $required = $this->minlength || $this->required ? "required" : "";
        $str = "";

        if ($this->multiple) {
            $n = $this->name . "[]";
            $t = "select multiple";
        } else {
            $n = $this->name;
            $t = "select";
        }
        $str .= "<$t $required class='$this->classname' name='$n' id='$n'";
        if ($this->size)
            $str .= " size='$this->size'";
        if ($this->extrahtml)
            $str .= " $this->extrahtml";
        $str .= ">";

        reset($this->options);
        while (list($k, $o) = each($this->options)) {
            $str .= "<option";
            if (is_array($o))
                $str .= " value='" . $o["value"] . "'";
            if (!$this->multiple && ($this->value == @$o["value"] || @$this->value == $o))
                $str .= " selected='selected'";
            elseif ($this->multiple && is_array($this->value)) {
                reset($this->value);
                while (list($tk, $v) = each($this->value)) {
                    if ($v == $o["value"] || $v == $o) {
                        $str .= " selected";
                        break;
                    }
                }
            }
            $str .= ">" . (is_array($o) ? $o["label"] : $o) . "</option>\n";
        }
        $str .= "</select>";

        $count = 1;
        return $str;
    }

    function self_get_frozen($val, $which, &$count)
    {
        $str = "";

        $x = 0;
        $n = $this->name . ($this->multiple ? "[]" : "");
        $v_array = (is_array($this->value) ? $this->value : array($this->value));
        #$str .= "<table border=1>\n";
        reset($v_array);
        while (list($tk, $tv) = each($v_array)) {
            reset($this->options);
            while (list($k, $v) = each($this->options)) {
                if ((is_array($v) &&
                        (($tmp = $v["value"]) == $tv || $v["label"] == $tv))
                    || ($tmp = $v) == $tv) {
                    $x++;
                    $str .= "<input class='$this->classname type='input' disabled name='$n' value='" . $v["label"] . "'>";
                    #$str .= "" . (is_array($v) ? $v["label"] : $v) . "";
                }
            }
        }
        $str .= "\n";

        $count = $x;
        return $str;
    }

    function self_get_js($ndx_array)
    {
        $str = "";

        if (!$this->multiple && $this->valid_e) {
            $str .= "if (f.$this->name.selectedIndex == 0) {\n";
            $str .= "  alert(\"$this->valid_e\");\n";
            $str .= "  f.$this->name.focus();\n";
            $str .= "  return(false);\n";
            $str .= "}\n";
        }

        return $str;
    }

    function self_validate($val)
    {
        if (!$this->multiple && $this->valid_e) {
            reset($this->options);
            $o = current($this->options);
            if ($val == @$o["value"] || @$val == $o) return $this->valid_e;
        }
        return false;
    }

} // end SELECT


