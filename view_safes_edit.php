<?php
########################################################################
# Copyright 2001 Desktop Solutions Software, Inc.
# 631-493-3422
# info@desktopsolutions.com
# www.desktopsolutions.com
# usage rights granted for use on a per-project-basis
#########################################################################
require( "php/prepend.php" );
page_open(
	array(
		"sess" => "DS_Session",
		"auth" => "DS_Auth",
		"perm" => "DS_Perm"
	) );
$form = new DSForm;

$tablename = "view_safes";
$form->add_element( array(
	"type"  => "hidden",
	"name"  => "tablename",
	"value" => $tablename
) );
if ( ! $_REQUEST['key'] ) {
	$CurrentRecord = ReadCurrentRecord( "$tablename", "idcol", $_REQUEST['itemnum'] );
	$key           = $CurrentRecord->idcol;
}

$CurrentRecord = ReadCurrentRecord( "$tablename", "idcol", $key );
$smarty->assign("CurrentRecord",$CurrentRecord);

$smarty->assign("last_safe_no",OneSQLVALUE("SELECT MAX(serialnumber) FROM safemast"));


//Set up fields here
if ( isset( $key ) ) {
	$form->add_element( array(
		"type"  => "hidden",
		"name"  => "key",
		"value" => $key
	) );
}
if ( isset( $delete ) ) {
	$form->add_element( array(
		"type"  => "hidden",
		"name"  => "delete",
		"value" => 1
	) );
}
if ( isset( $returnto ) ) {
	$form->add_element( array(
		"type"  => "hidden",
		"name"  => "returnto",
		"value" => $returnto
	) );
}

$smarty->assign("serial_numeric",substr( $CurrentRecord->serialnumber, 3 ));

$Validator = ReadCurrentRecord("validator","safeid",$CurrentRecord->serialnumber);
$deviceid = $Validator->deviceid;
$smarty->assign("device_numeric",substr( $deviceid, 4 ));

# the bank IDS are currently hard coded in the smarty template.
$stores_select = ReturnDropDownHTML( "select CONCAT(storecode,' ',store_name,' - ',f_address,' ',f_city),storecode from store order by 1", "field_storecode",$CurrentRecord->storecode);
#this listbox is drawn in the smarty template
$smarty->assign("stores_select",$stores_select);

$usercode  = OneSQLValue( "SELECT userid FROM user WHERE storecode='$storecode'" );
$user_select = ReturnDropDownHTML( "SELECT CONCAT(userid,' - ',password),userid FROM user WHERE storecode='$storecode' AND password> '' ORDER BY 1" ,"usercode",$usercode);
#this listbox drawn in the smarty template
$smarty->assign("user_select",$user_select);

//if being called from myself, do validation..
if ( @$Submit ) {
	// paste on the 'SA and S&S' pieces of the serial number and the device ID
	$_REQUEST['deviceid'] = 'S&S-'.$_REQUEST['deviceid'];
	$_REQUEST['field_serialnumber'] = 'SA-'.$_REQUEST['field_serialnumber'];
	$_REQUEST['field_lckuser'] = $_REQUEST['field_adduser'] = "WEB";
	$_REQUEST['field_lckstat'] = "";
	$_REQUEST['field_manufacturer'] = 'S and S';
	$_REQUEST['field_send_email_summary'] = 'Y';

	$_REQUEST['field_adddate'] = $_REQUEST['field_lckdate'] =  date('Y-m-d H:i:s');

	$validator['field_deviceid']=$_REQUEST['deviceid'];
	$validator['field_safeid']=$_REQUEST['field_serialnumber'];
	$validator['field_adddate'] = $validator['field_lckdate'] =  date('Y-m-d H:i:s');
	$validator['field_lckstat'] = "";
	$validator['field_lckuser'] = $validator['field_adduser'] = "WEB";

	if (! $key && OneSQLValue("SELECT 1 FROM safemast WHERE serialnumber='{$_REQUEST[field_serialnumber]}'")) {
		die("Safe serial number {$_REQUEST[field_serialnumber]} is already in use - press the back button and try again");
	}
	if (! $key && OneSQLValue("SELECT 1 FROM validator WHERE deviceid='{$_REQUEST[deviceid]}'")) {
		die("Validator with number {$_REQUEST[deviceid]} is already in use - press the back button and try again");
	}

	// send to SQL
	if ( strlen( $_REQUEST['new_email'] && strlen ($_REQUEST['new_password'] ) ) ) {
		// conditionally insert a record into the user table
		if (! OneSQLValue("SELECT 1 FROM user where emailaddress='{$_REQUEST[new_email]}' ")) {
			$userdata['field_userid'] = $userdata['field_emailaddress'] = $_REQUEST['new_email'];
			$userdata['field_password'] = $_REQUEST['new_password'];
			$userdata['field_storecode'] = $_REQUEST['field_storecode'];
			$userdata['field_compcode'] = substr($_REQUEST['field_storecode'],5);
			$userdata['field_adddate'] = $userdata['field_lckdate'] =  date('Y-m-d H:i:s');
			$userdata['field_lckstat'] = "";
			$userdata['field_lckuser'] = $userdata['field_adduser'] = "WEB";
			$Store = ReadCurrentRecord("store","storecode",$_REQUEST['field_storecode']);
			$userdata['field_username'] = "$Store->store_name #$Store->storecode";
			$userdata['field_isactive'] = "1";
			$userdata['field_access_safe_data'] = "Y";
			$userdata['field_regioncode'] = "";
			$userdata['field_sequencestops'] = "N";
			DoInsert($userdata,"user");
		}
		$User = ReadCurrentRecord("user","emailaddress",$_REQUEST['new_email']);
	}
	else {
		$User = ReadCurrentRecord("user","userid",$_REQUEST['usercode']);
		// always set the menu flag option for these users to Y
		DoQuery("UPDATE user SET access_safe_data='Y' WHERE userid='".$_REQUEST['usercode']."'");
	}
	$_REQUEST['field_email_address_list'] = "Jameskelly@ssarmored.com";

	if (stristr($User->emailaddress,'@')) {
		$_REQUEST['field_email_address_list'] .= ",$User->emailaddress";
	}
	if ( isset( $key ) ) {
		// UPDATE
		if ( ! isset( $delete ) ) {
			DoUpdate($validator,"validator","idcol",$Validator->idcol);
			$retval = DoUpdate( $_REQUEST, "safemast", "idcol", $key );
		} else {
			// DELETE, CHECK FOR RELATED RECORDS IF REQUIRED
			#DoDelete( "$tablename", "idcol", $key );
		}
	} else {
		DoInsert($validator,"validator");
		$retval = DoInsert( $_REQUEST, "safemast" );
	}
	if ( $retval ) {
		$form->SaveAndReturn( "safemast", "idcol", $key );
	}
	return;

}

$form->template="view_safes_edit.tpl";
//Begin HTML here
DSBeginPage();
$form->StartForm( "Detail - Safe record" );
if ( isset( $delete ) || isset( $view ) ) {
	$form->freeze();
}
if ( isset( $delete ) ) {
	echo "Because there may be transactions, safes can not be deleted here.";
	//don't print javascript
	$form->jvs_name = "";
}
// All this HTML is output in smarty template view_sales_edit_contents.tpl and so has been commented out here -- go look there for it
/*
$form->DrawField( "field_serialnumber", "Serial #", 1, 0, 0 );
$form->DrawField( "deviceid", "Validator ID", 1, 0, 0 );
$form->DrawField( "field_bankfedid", "Bank", 1, 0, 0 );
$form->DrawField( "field_storecode", "Store Code", 1, 0, 0 );
$form->DrawField( "usercode", "Existing User", 1, 0, 0 );
$form->DrawTextRow( "- or -", "", 0, 0, 0 );
$form->DrawField( "new_email", "New User Email", 1, 0, 0 );
$form->DrawField( "new_password", "New Password", 1, 0, 0 );
*/
$form->EndForm( ! isset( $key ) );


// Save data back to database.
DSEndPage();
?>
