<?php
########################################################################
# Copyright 1999 Desktop Solutions Software, Inc.
# 516-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
require( "php/prepend.php" );
page_open(
array("sess" => "DS_Session",
"auth" => "DS_Auth",
"perm" => "DS_Perm"));
#########################################################################
# Browse routine for shogyo.modularjacks
# generated at Thursday 05th of April 2001 01:32:36 PM
##############
$t = new DSBrowse;
$db = new DB_Example;
$t->Key = "idcol"; //must match case used in select statement!
$t->classname = "view_safes";
$t->TableName = "view_safes";
$t->Columns = array("safeid","deviceid","store_name","f_city","f_state","idcol");
$t->TopHeading= "Safe Records";
$t->fields=array("Safe ID","Validator ID","Store","City","State");
$t->SearchChoices = array("Safe ID"=>"safeid","Validator ID"=>"deviceid","Store Name"=>"store_name");
$t->DisplayRows=25;
## Optional settings:
DSBeginPage("Safe Browse");
$t->display();
DSEndPage();
?>
