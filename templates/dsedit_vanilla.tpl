<!DOCTYPE html>
<html>

{include file="open.tpl"}

<body>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center m-t-lg">
                        {include file="dsedit_contents.tpl"}
                    </div>
                </div>
            </div>
        </div>

{include file="close.tpl"}

</body>

</html>
