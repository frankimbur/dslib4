        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{$topheading}</h5>
                        <div class="ibox-tools">
                            {if $searchoptions}
                            <form action="{$scriptname}" method="get"</form><select name="searchby" style="width: 200px;height: 24px;">{$searchoptions}</select>&nbsp;<input class="inputsm" value="{$searchfor}" name="searchfor" size="20"><button class='btn btn-primary ds-edit-btn' style='width: 70px;height: 25px;margin-top: -4px;' type='submit'>Search</button></form>
                            {/if}
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTable-{$classname} table-browse-{$classname}?>" >
                                <thead>
                                <tr>
                                    {foreach from=$fields item=column}
                                        <th class='table-browse-heading-{$classname}-{$column}'>{$column}</th>
                                        {if $column@iteration >= $hidefrom}
                                            {break}
                                        {/if}
                                    {/foreach}
                                    <td nowrap='on' class='text-center'>{$addbutton}</td>
                                </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
