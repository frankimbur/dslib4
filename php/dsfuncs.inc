<?php

class DB_Example {
    // stub for backwards compatibility
}

function DSBeginPage($title="") {
    global $DSTITLE,$DSPRODUCTTHUMBDIR,$DSPRODUCTTHUMBURL,$DSUPLOADDIR,$DSUPLOADURL;
    if (! empty($_REQUEST['draw']) ) { // this indicates we are being called by Ajax , so we pass control to the Ajax handler. We do it this way so that all the code for the browse function exists before the Ajax handler executes.
        return;
    }
    if (!$title) {
        $title = $DSTITLE;
    }
    global $smarty;
    $smarty->assign("DSTITLE",$title);
	$smarty->assign("DSPRODUCTTHUMBDIR",$DSPRODUCTTHUMBDIR);
	$smarty->assign("DSPRODUCTTHUMBURL",$DSPRODUCTTHUMBURL);
	$smarty->assign("DSUPLOADDIR",$DSUPLOADDIR);
	$smarty->assign("DSUPLOADURL",$DSUPLOADURL);

    ob_start();
}
######################
function DSEndPage($template=NULL)
######################
{
    if (! empty($_REQUEST['draw']) ) { // this indicates we are being called by Ajax , so we pass control to the Ajax handler. We do it this way so that all the code for the browse function exists before the Ajax handler executes.
        return;
    }
    global $smarty,$DSTEMPLATE;
    if ($template) {
        $DSTEMPLATE = $template;
    }
    $contents=ob_get_contents();
    // any echo statements that are made between DSBeginPage and DSEndPage will be captured here and sent to smarty as the $contents variable
    // the default.tpl template will render those echo statements in the main content area
    // other templates may provide their own content for that area
    ob_end_clean();
    $smarty->assign("contents",$contents);
    if (empty($DSTEMPLATE)) {
        $DSTEMPLATE="default.tpl";
    }
	try {
		$smarty->display( $DSTEMPLATE );
	} catch ( SmartyException $e ) {
	} catch ( Exception $e ) {
	}
}
#############
function OneSQLValue($sql)
#############
# returns a single column from a SQL query
# blank string if not found
{
    $result=DoQuery($sql);
    if ($result) {
        $Record=mysqli_fetch_array($result);
        mysqli_free_result($result);
        return $Record[0];
    }
    else {
        return $result;
    }
}
#####################
Function DoUpdate($vars,$table,$key,$keyval,$isnum=0)
## Formats & Sends an update command when field.. vars are filled in
######################
{
    $key = stripslashes($key);
    $keyval=addslashes(stripslashes($keyval));

    $valuelist = '';
    while (list($field, $val) = each($vars)) {
        if ( substr($field,0,6)=="field_") {
            $field=substr($field,6);
            $valuelist .= "$field = '".addslashes(stripslashes($val))."', ";
        }
    }
    $valuelist = substr($valuelist,0,strlen($valuelist)-2);

    if (!$isnum)
        $query = "UPDATE $table SET $valuelist WHERE $key = '$keyval'";
    else
        $query = "UPDATE $table SET $valuelist WHERE $key = $keyval";

    $retval=DoQuery($query);
    return $retval;
}
//*****
Function DoDelete($table,$key,$keyval)
// Formats and sends a delete command for a table with a key field & value
//*****
{
    $keyval=addslashes(stripslashes($keyval));
    $query = "DELETE FROM $table WHERE $key = '$keyval'";
    return DoQuery($query);
}
###############
Function DoInsert($vars,$table)
## Formats & Sends an insert command when field.. vars are filled in
###############
{
    global $DSLINK;
    $fieldlist = '';
    $valuelist = '';
    while (list($key, $val) = each($vars)) {
        if ( strstr($key,"field_")) {
            $key=substr($key,6);
            $fieldlist .= "$key, ";
            $valuelist .= "'".addslashes(stripslashes($val))."', ";
        }
    }
    $fieldlist = preg_replace('/, $/', '', $fieldlist);
    $valuelist = preg_replace('/, $/', '', $valuelist);
    $query = "INSERT INTO $table ($fieldlist) VALUES ($valuelist)";
    $retval=DoQuery($query);
    if ($retval) {
        $retval=mysqli_insert_id($DSLINK);
    }
    return $retval;

}
//*****
Function DoQuery($query)
// Sends query to database engine, returns error if required
// returns result code
//*******
{
    global $DSDEBUG,$DSDATABASE,$DSHOST,$DSUSER,$DSPASSWORD,$DSLINK;
    if ($DSDEBUG)
        echo "DEBUG:<BR>".$query."<BR>";

    $DSLINK=mysqli_connect($DSHOST,$DSUSER,$DSPASSWORD,$DSDATABASE);
    if (!$DSLINK) {
    	die('mysql error '. mysqli_error($DSLINK));
    }
    $result=mysqli_query($DSLINK,$query);

    if (! $result) {
        $msg=mysqli_error($DSLINK);
        # check for duplicate error message
        if (strstr($msg,"Duplicate")) {
            echo "<H3>That is a duplicate entry.<BR>
                This data has not been saved.<BR>
                Please re-enter your information.</H3>
                ";
            if ($DSDEBUG) {
                echo $msg;
            }
        }
        else {
            global $HTTP_HOST;
            if (strstr($HTTP_HOST,"local") || $DSDEBUG) {
                echo "
                    <h3>Data has not been written!</h3>
                    The database engine reported the following error:<br><i>
                    $query";
                echo mysqli_error($DSLINK);
                echo
                "
                    </i><br><br>The following information may be helpful for debugging:<br><i>
                    $query</i>
                    <a href=$PHP_SELF>use the back button</A>
                    ";
	            die();
            }
            else {
                ErrorEmail($query);
            }
        }
    }
    return $result;
}
###############
function ReadCurrentRecord($table,$primary_key,$keyval,$key2=NULL,$val2=NULL,$key3=NULL,$val3=NULL)
###############
// Reads current record into $CurrentRecord object
{
    if (isset($keyval) && isset($val3))  {
        $query =
            sprintf("SELECT * FROM $table WHERE $primary_key = '%s' AND $key2='%s' AND $key3='%s'",
                addslashes($keyval),addslashes($val2),addslashes($val3));
        $result = DoQuery($query);
        $CurrentRecord = mysqli_fetch_object($result);
        $arr = @get_object_vars($CurrentRecord);
        if (sizeof($arr)) {
            while (@list($prop, $val) = each($arr))
                $CurrentRecord->$prop = stripslashes($val);
        }
        @mysqli_free_result($result);
    }
    if (isset($keyval) && isset($val2))  {
        $query =
            sprintf("SELECT * FROM $table WHERE $primary_key = '%s' AND $key2='%s'",
                addslashes($keyval),addslashes($val2));
        $result = DoQuery($query);
        $CurrentRecord = mysqli_fetch_object($result);
        $arr = @get_object_vars($CurrentRecord);
        if (sizeof($arr)) {
            while (@list($prop, $val) = each($arr))
                $CurrentRecord->$prop = stripslashes($val);
        }
        @mysqli_free_result($result);
    }
    elseif (isset($keyval)) {
        $myprimary_key = stripslashes($primary_key);
        $query ="SELECT * FROM $table WHERE $myprimary_key = '$keyval'";
        $result = DoQuery($query);
        $CurrentRecord = mysqli_fetch_object($result);
        $arr = @get_object_vars($CurrentRecord);
        if (sizeof($arr)) {
            while (@list($prop, $val) = each($arr))
                $CurrentRecord->$prop = stripslashes($val);
        }
        @mysqli_free_result($result);
    }
    else {
        $result = DoQuery("SHOW FIELDS FROM $table");
        $CurrentRecord = mysqli_fetch_object($result);
        mysqli_free_result($result);
    }
    return @$CurrentRecord;
}
####################
Function ForceSSL()
####################
{
    global $HTTP_HOST;
    if(!isset($_SERVER['HTTPS'])) {
        $domain=GetDomain($HTTP_HOST);
        $new = "https://$domain/".$_SERVER['SCRIPT_NAME'].'?'.$_SERVER['QUERY_STRING'];
        header("Location: $new");
        exit();
    }
}
#########################
function GetDomain($url)
########################
{
    $nowww = str_replace('www\.','',$url);
    $domain = parse_url($nowww);
    if(!empty($domain["host"])) {
        return $domain["host"];
    } else {
        return $domain["path"];
    }
}
// FTI stubs to obsolete much of phblib BEGIN
function page_open($feature) {
    global $_PHPLIB,$auth;

    # enable sess and all dependent features.
    if (isset($feature["sess"])) {
        global $sess;
        $sess = new $feature["sess"];
        $sess->start();

        # the auth feature depends on sess
        if (isset($feature["auth"])) {
            global $auth;

            if (!isset($auth)) {
                $auth = new $feature["auth"];
            }
            $auth->start();

            # the perm feature depends on auth and sess
            if (isset($feature["perm"])) {
                global $perm;

                if (!isset($perm)) {
                    $perm = new $feature["perm"];
                }
            }

            # the user feature depends on auth and sess
            if (isset($feature["user"])) {
                global $user;

                if (!isset($user)) {
                    $user = new $feature["user"];
                }
                $user->start($auth->auth["uid"]);
            }
        }

        ## Load the auto_init-File, if one is specified.
        if ((@$sess->auto_init != "") && (@$sess->in == "")) {
            $sess->in = 1;
            include($_PHPLIB["libdir"] . $sess->auto_init);
            if ($sess->secure_auto_init != "") {
                $sess->freeze();
            }
        }
    }
}

function page_close() {
    global $DSLINK;
    @mysqli_close($DSLINK);
}


class Session {
    function start()
    {
        @session_start();
    }
    function register($var)
    {
        $_SESSION["$var"]=$GLOBALS["$var"];
    }
    function self_url()
    {
        return "";
    }
    function freeze()
    {

    }
}

class User {
}

class Perm {
    function check($checkfor){
        if (! HasPermissions($checkfor)) {
            die('no access...');
        }
    }
}
class Auth {
	var $loginpage='php/loginform-default.html'; //over-ride this value in children to display another login page
    function Login($username,$password) //this can also be called directly to login
    {
        global $smarty;
        $sql= sprintf("select uid,perms from auth_user where length(username) and username = '%s' and decode(password,'post') = '%s' and active='Y'",$username,$password);
        $r=DoQuery($sql);
        if ( mysqli_num_rows($r)) { // matching user id
            $UserRecord=mysqli_fetch_object($r);
            $_SESSION['uid']=$UserRecord->uid; //store userid in session, we're now logged in
            $this->auth['uid']=$UserRecord->uid; //backwards compatibility with old structure
            return TRUE;
        }
        return FALSE;
    }
    function start()
    {
        if (strlen($_SESSION['uid'])) { // user is logged in
            $this->auth['uid']=$_SESSION['uid']; //backwards compatibility with old structure
            return; //allow program to continue;
        }
        if (sizeof($_POST)) { // user is trying to log in
            if ($this->Login($_POST[username],$_POST[password]) ) {
                $url = $_SERVER['SCRIPT_NAME'].'?'.$_SERVER['QUERY_STRING'];
                Header("Location:  $url"); // SUCCESS -- reload this page
                exit();
            }
            global $username;
            $username=$_POST[username]; // when the loginform sees a value here it will display an error message
        }
        DSBeginPage("Log In");
        include ($this->loginpage);
        DSEndPage();
        exit();
    }
    function logout()
    {
        unset($_SESSION['uid']);
        unset($_SESSION['lastcache']);
        unset($GLOBALS['USER_RECORD']);
    }
    function url()
    {
    }
}
class DS_Session extends Session {
}

class DS_Auth extends Auth {
}

class DS_Default_Auth extends DS_Auth {
}

class DS_Perm extends Perm {
}
// FTI END
#####################
Function GetUserId()
#####################
{
    return GetSessionID();
}
#####################
Function GetUsersRecord()
#####################
{
    global $UserRecord;
    if (! @$UserRecord->uid ) {
        $UserRecord = ReadCurrentRecord("auth_user","uid",GetSessionID());
    }
    return $UserRecord;
}
#####################
Function GetSessionID()
#####################
{
    return @$_SESSION['uid'];
}
Function HasPermissions($perm)
{
	$User= GetUsersRecord();
	return stristr($perm,$User->perms);
}
##################
Function DumpSql($sql,$nototals=0)
##################
{
    $result=DoQuery($sql);
    $total_rows = mysqli_num_rows($result);
    $row = mysqli_fetch_row($result);
    $total_cols = count($row);
    print "<table width='500' border='1' cellspacing='0' cellpadding='1' align='center'>";

    print "<tr>";
    $i=0;
    while($i < $total_cols) {
        print "<th><font size=2>";
        print mysqli_field_name($result,$i);
        print "</th>";
        $i++;
    }
    print "</tr>";

    print "<tr>";
    $i=0;
    while($i < $total_cols) {
        print "<td nowrap valign=top><font size=2>";
        print $row[$i];
        print "</td>";
        $i++;
    }
    print "</tr>";

    while($row = mysqli_fetch_row ($result)) {
        $i = 0;
        print "<tr>";
        while($i < $total_cols){
            print "<td nowrap valign=top><font size=2>";
            print $row[$i];
            print "</td>";
            $i++;
        }
        print "</tr>";
    }
    if (!$nototals) {
        print "<tr>";
        $i=0;
        while($i < $total_cols){
            print "<th nowrap valign=top><font size=2>";
            print "</th>";
            $i++;
        }
        print "</tr>";
    }
    print "</TABLE>";
}
function mysqli_field_name($result, $field_offset)
{
    $properties = mysqli_fetch_field_direct($result, $field_offset);
    return is_object($properties) ? $properties->name : null;
}

###############
Function DrawDropDownHTML($sql,$name,$default,$pleaseselect=0)
###############
# Draws HTML for a drop down from a code table with two fields
# for when form class is not being used
# Supplies default choice, too
# Example SQL - SELECT Data,Code from codetable order by Data
{
    $result=DoQuery($sql);
    $cols = mysqli_num_fields($result);
    echo "<select NAME=$name>\r";
    if ($pleaseselect==1) {
        echo "<OPTION value=\"\">-No Preference-";
    }
    while ($Record=mysqli_fetch_array($result))  {
        $data0=trim($Record[0]);
        $data1=trim($Record[1]);
        echo "<OPTION ";
        if ($data1 == $default) {
            echo " SELECTED ";
        }
        echo "VALUE=\"$data1\">$data0";
    }
    mysqli_free_result($result);
    echo "</select>\r";
}
###############
Function ReturnDropDownHTML($sql,$name,$default,$pleaseselect=0)
###############
# SAME FUNCTION AS ABOVE, but returns code in a string rather than echo
# Draws HTML for a drop down from a code table with two fields
# for when form class is not being used
# Supplies default choice, too
# Example SQL - SELECT Data,Code from codetable order by Data
{
    $returnstring = "";
    $result=DoQuery($sql);
    $cols = mysqli_num_fields($result);
    $returnstring.= "<select class='form-control' NAME='$name' ID='$name'>\r";
    if ($pleaseselect==1) {
        $returnstring.= "<OPTION value=\"\">--None--";
    }
    while ($Record=mysqli_fetch_array($result))  {
        $data0=trim($Record[0]);
        $data1=trim($Record[1]);
        $returnstring.= "<OPTION ";
        if ($data1 == $default) {
            $returnstring.= " SELECTED ";
        }
        $returnstring.= "VALUE=\"$data1\">$data0";
    }
    mysqli_free_result($result);
    $returnstring.= "</select>\n";
    return $returnstring;
}
###########
Function CalcDetailListImageSize(&$x,&$y,$img)
##########
{
	global $DSUPLOADDIR;
	$file = $DSUPLOADDIR.'/'. "$img";
	$finalsize=315; //$DSTHUMBSIZE; #change this to resize photos..
	if (file_exists($file)) {
		$Size=GetImageSize($file);
		$Width=$Size[0];
		$Height=$Size[1];
		if ($Width > $Height) {
			#wide
			$x=$finalsize;
			$ratio=$finalsize/$Width;
			$y=$Height*$ratio;
		}
		if ($Height > $Width) {
			#tall
			$y=$finalsize;
			$ratio=$finalsize/$Height;
			$x=$Width*$ratio;
		}
		if ($Height == $Width) {
			#square
			$x=$finalsize;
			$y=$finalsize;
			$y=$finalsize;
			$ratio=$finalsize/$Height;
			$x=$Width*$ratio;
		}
	}
	$x=floor($x);
	$y=floor($y);
	if ($x>$Width && $y>$Height) {
		$x=$Width;
		$y=$Height;
	}
	return;
}
###########
Function CalcListImageSize($x,$y,$img)
##########
{
	global $DSUPLOADDIR;
	$file = $DSUPLOADDIR . "/$img";
	$finalsize=250; #change this to resize photos..
	if (file_exists($file)) {
		$Size=GetImageSize($file);
		$Width=$Size[0];
		$Height=$Size[1];
		if ($Width <= $finalsize || $Height <= $finalsize)
			return;
		if ($Width > $Height) {
			#wide
			$x=$finalsize;
			$ratio=$finalsize/$Width;
			$y=$Height*$ratio;
		}
		if ($Height > $Width) {
			#tall
			$y=$finalsize;
			$ratio=$finalsize/$Height;
			$x=$Width*$ratio;
		}
		if ($Height == $Width)
			#square
			$x=$finalsize;
		$y=$finalsize;
		$y=$finalsize;
		$ratio=$finalsize/$Height;
		$x=$Width*$ratio;
	}
	$x=floor($x);
	$y=floor($y);
	echo "(".$x."X".$y.")";
	return;
}
##################
function ExecReport($cmd)
##################
{
	system($cmd,$result);
}
##################
function CreateSizedImages($file)
##################
# Creates appropriate sized images in small and large directories
{
	global $DSUPLOADDIR,$DSPRODUCTTHUMBDIR;
	$source = $DSUPLOADDIR."/".$file;
	$smalldest = $DSPRODUCTTHUMBDIR."/".$file;
	CalcDetailListImageSize($w,$h,$file);

	list($width, $height) = getimagesize($source);
	$r = $width / $height;
	if ($w/$h > $r) {
		$newwidth = $h*$r;
		$newheight = $h;
	} else {
		$newheight = $w/$r;
		$newwidth = $w;
	}
	if (stristr($source,".jpg") || stristr($source,".jpeg"))
		$src = imagecreatefromjpeg($source);
	elseif (stristr($source,".gif"))
		$src = imagecreatefromgif($source);
	elseif (stristr($source,".png"))
		$src = imagecreatefrompng($source);
	else
		die('CreateSizedImage - cant handle file $src');
	$dst = imagecreatetruecolor($newwidth, $newheight);
	imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

	if (stristr($source,".jpg") || stristr($source,".jpeg"))
		imagejpeg($dst,$smalldest);
	elseif (stristr($source,".gif"))
		imagegif($dst,$smalldest);
	elseif (stristr($source,".png"))
		imagepng($dst,$smalldest);
	return $smalldest;
}
########
Function GenThumbIfNeeded($in)
########
# returns NULL if no thumb needs to be generated
# returns a filename of a generated file if one was created
{
	global $DSUPLOADDIR,$DSPRODUCTTHUMBDIR;
	if (file_exists($DSUPLOADDIR.'/'.$in) &&
	    ! file_exists($DSPRODUCTTHUMBDIR.'/'.$in) ) {
		return CreateSizedImages($in);
	}
	return NULL;
}
#########
Class DSManyBox
#########

{
	var $Key;
	#        must be a column name, spelled just like in select statement
	var $Columns = "";
	#        ie "first,last,contact_no" - will be used in select statement
	var $TopHeading = "Data Listing";
	#       appears at top of browse box
	var $TableName;
	#       sql tablename
	var $WhereClause = "1=1";
	#        ie "salary > 0" - will be tacked on to every where clause built
	var $ChangeOrder = 0;
	#       a flag which will will show arrows to adjust order (requires helper script -- changeorder.php and a value in ORderBy)
	var $EditUrl;
	#       the script url that will be linked to the
	#       edit button. will also be called for delete and edit with $key and $delete
	var $OrderBy = "1";
	#       an optional orderby clause - do not include 'order by' defaults to '1'
	#       if the user changes the searchby field, this will be changed

	var $Functions;
	#       an array which can hold keyed pairs of column names and functions to
	#       call which will then be responsible for displaying that column.
	#       The function will be called with a single parameter, this browse object
	#       EXAMPLE:
	#       $t->Functions=Array("stats"=>"displaystats");
	#       ######
	#       Function displaystats($CurrentRecord,$color)
	#       ######
	#       {
	#           if ($CurrentRecord[player_type]=='pitcher'))
	#               $data=$CurrenRecord[era];
	#           else
	#               $data=$CurrentRecord[battingaverage];
	#           echo "<TD NOWRAP BGCOLOR=$color>$data</TD>";
	#       }
	var $ParentKeyVal;
	#    value of the key in the parent table that distinguishes these child records

	var $NoAdding;

	# Flag set to 1 to suppress the add button

	var $scriptname;// will be set to $_SERVER['REQUEST_URI'];

	function __construct() {
		$this->scriptname= $_SERVER['SCRIPT_NAME'].'?'.$_SERVER['QUERY_STRING'];
	}


	FUNCTION Display() {
		//build SQL statment
		$sql     = "SELECT $this->Columns,$this->Key FROM $this->TableName WHERE $this->WhereClause ORDER BY $this->OrderBy";
		$r       = DoQuery( $sql );
		$numcols = mysqli_num_fields( $r );
		// display heading, start table
		if ( ! $this->NoAdding ) {
			echo "
                <table class='table table-striped'>
                <tr>
                <th COLSPAN=" . ( $numcols - 1 ) . "><span class=DSFORM_label>$this->TopHeading</span></th>
                <td align=right><a class=\"btn btn-primary\" style=\"width:70px;\" role=\"button\" href=$this->EditUrl?field_$this->ParentKey=$this->ParentKeyVal>Add</i></a></td>
                </tr>
                ";
		} else {
			echo "
                <table class='table table-striped'>
                <tr>
                <td COLSPAN=" . ( $numcols - 1 ) . "><span class=DSFORM_label>$this->TopHeading</span></td>
                <td align=right>&nbsp;</a></td>
                </tr>
                ";
		}
		// loop through records
		while ( $MBRecord = mysqli_fetch_array( $r ) ) {
			$keyval = $MBRecord["$this->Key"];
			// sanity check; do we have a key?
			if ( empty( $keyval ) ) {
				die( "fatal error in DSManyBox->display, key field ($this->Key) not set properly.." );
			}
			echo "
                <tr>
                ";
			if ( $this->ChangeOrder ) {
				$arrowbuttons = "<a href=changeorder.php?tablename=$this->TableName&orderby=$this->OrderBy&key=$keyval&keyfield=$this->Key&wc=$this->WhereClause&dir=up><i class=\"fa  fa-2x fa-sort-asc btn btn-primary\"></i></a>&nbsp;<a href=changeorder.php?tablename=$this->TableName&key=$keyval&keyfield=$this->Key&orderby=$this->OrderBy&wc=$this->WhereClause&dir=down><i class=\"btn btn-primary fa  fa-2x fa-sort-desc\"></i></a>";
			}
			// added stripslash FTI 7/3/07
			for ( $i = 0; $i < $numcols - 1; $i ++ ) {
				$colname              = mysqli_field_name( $r, $i );
				$MBRecord[ $i ]       = stripslashes( stripslashes( $MBRecord[ $i ] ) );
				$MBRecord[ $colname ] = stripslashes( stripslashes( $MBRecord[ $i ] ) );
			}
			// loop thru columns
			for ( $i = 0; $i < $numcols - 1; $i ++ ) {
				// check for a custom function set for this column and execute it...
				$colname = mysqli_field_name( $r, $i );
				if ( strlen( $this->Functions[ $colname ] ) ) {
					$functioncall = $this->Functions[ $colname ] . "(\$MBRecord,\$color);";
					eval( $functioncall );
				} else {
					echo "<td >" . stripslashes( $MBRecord[ $i ] ) . "</td>";
				}
			}

			echo "<td NOWRAP WIDTH=10%>$arrowbuttons <a class=\"btn btn-primary\" style=\"width:70px;\" role=\"button\" href=$this->EditUrl?key=$keyval>Edit</a>&nbsp;<a class=\"btn btn-primary\" style=\"width:70px;\" role=\"button\" href=$this->EditUrl?key=$keyval&delete=1>Delete</a></td>";

			echo
			"</tr>";
		}
		echo "
            </table>
            ";
		$_SESSION['dsbrowse'] = serialize($this);
	}

}

//*******
function RICheck($table,$field,$keyval)
//*******
{
	$RImsg="";
	$query = sprintf("SELECT count(*) FROM %s where %s='%s'",$table,$field,$keyval);
	$count = OneSQLValue($query);
	if ($count) {
		$RIMsg =
			sprintf("That record is used %d time(s) in the <i>%s</i> table.<br>",
				$count,
				$table);
		return $RIMsg;
	}
	else {
		return "";
	}
}
//************
function ReportRIError($errmsg,$table,$keyval)
//************
{
	DSBeginPage( "Error" );
	echo( "<H2>$errmsg</H2>" );
	echo( "ERROR: In order to delete this record from the <i>$table</i> table you must first find and delete all the above records." );
	DSEndPage();
	die();
}
