<?php
//todo production and dev environments
#error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

if (phpversion() >= 5.4 && empty($_REQUEST['draw'])) { // this indicates we are being called by Ajax , the register_globals in here messes this up, so skip it
    // this file provides register globals for backwards compatibility
    require_once('php_compat_54.inc');
}

// load the config file stored in the private folder
require_once ("config.inc");


// load the smarty library & create a master global class $smarty
require_once('vendor/smarty/libs/bootstrap.php');
global $smarty;
$smarty = new Smarty();
$smarty->setCacheDir("templates_c");

// load the DSLib master include which sets up DSBrowse and DSForm

require_once('dsfuncs.inc');

// register the DSBrowse class
require_once('dsbrowse.inc');

// register the DSForm class
require_once('oohforms.inc');
require_once('dsform.inc');

// if available load the wordpress code and save if we are logged in
// wordpress steps on the REQUEST super global and the library needs it so we stash and then restore it
if (file_exists($DSHOME.'/wp-config.php')) {
	$request_save_us=$_REQUEST;
	require_once($DSHOME.'/wp-config.php');
	$_REQUEST=$request_save_us;
	$smarty->assign("is_user_logged_in",is_user_logged_in());
}