<?php
########################################################################
# Copyright 2000 Desktop Solutions Software, Inc.
# 631-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
require( "php/prepend.php" );
session_destroy();
session_commit();
session_reset();
Header("Location:  index.php");
?>

