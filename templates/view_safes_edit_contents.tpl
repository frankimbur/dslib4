<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{$heading}</h5>
            </div>
            <div class="ibox-content">
                <form method="post" enctype="multipart/form-data" class="form-horizontal">
                    {if $delete}
                        <div class="form-group">
                            <div class="alert alert-danger">Press SUBMIT to delete this record...</div>
                        </div>
                    {/if}
                    {if $delete}
                    <fieldset disabled="disabled">
                        {/if}
                        {foreach from=$elements item=element}
                            {if $element['ob']->type=='of_hidden'}
                                <input type="hidden" name="{$element['ob']->name}" value="{$element['ob']->value}">
                            {/if}
                        {/foreach}
                        <input type="hidden" name="tablename" value="safemast">
                        <input type="hidden" name="key" value="{$CurrentRecord->idcol}">

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Safe ID<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                &nbsp;&nbsp;SA-<input name='field_serialnumber' value="{$serial_numeric}" id='field_serialnumber' type='number' class='form-control' required min=0 max=99999 style="display:inline!important;"/ placeholder='99999' pattern='[0-9]'><BR>
                                <div>Last Safe Used was: {$last_safe_no}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Validator ID<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                S&S-<input name='deviceid' value="{$device_numeric}" id='deviceid' type='number' class='form-control' required min=0 max=99999 style="display:inline!important;"/ placeholder='99999' pattern='[0-9]'>
                                <div >Note -- must be a 'Display ID' from the E2 system - <a target=_new href="http://api.e2c.io/Enterprise/System">http://api.e2c.io/Enterprise/System</a></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Bank<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <select required class='form-control' name='field_bankfedid' id='field_bankfedid'>
                                    <option>CCSMARTSAFE7</option>
                                    <option>SMARTSAFE</option>
                                    <option>10000000</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Store Code<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                    {$stores_select}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Existing User<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9" id="usercode_container">
                                {$user_select}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">

                                </label>
                            </div>
                            <div class="col-sm-9 control-label">
                                - or -
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    New User Email or ID<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input name='new_email' value="" id='new_email' type='text' class='form-control'
                                       size='50'/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    New Password<span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input name='new_password' value="" id='new_password' type='text' class='form-control'
                                       size='50'/>
                            </div>
                        </div>


                        {if $delete}
                    </fieldset>
                    {/if}
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-primary" type="submit" name="Submit" value="Submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<SCRIPT>
    $( "#field_storecode" ).change(function() {
        fnbuildValue(this.value);
    });
    $( "#field_serialnumber" ).change(function() {
        LeftPad(this);
    });
    $( "#deviceid" ).change(function() {
        LeftPad(this);
    });
    // these two functions are used to initiate AJAX
    function createRequestObject(){
        var request_o; //declare the variable to hold the object.
        var browser = navigator.appName; //find the browser name
        if(browser == "Microsoft Internet Explorer"){
            /* Create the object using MSIE's method */
            request_o = new ActiveXObject("Microsoft.XMLHTTP");
        }else{
            /* Create the object using other browser's method */
            request_o = new XMLHttpRequest();
        }
        return request_o; //return the object
    }

    var http = createRequestObject();

    function fnbuildValue(value) // a function to call the php script that will populate the placeholder DIV with a select drawn from the database
    {
        http.open('get', 'builduserlist.php?storecode='+escape(value));   //change this to call a script with a key -- that script echos the select contents
        http.onreadystatechange = getValue;    //'getValue' must match the function name below its a callback
        http.send(null);
    }
    function getValue() // a function that will actually accept the contents of the PHP script and set the contents of the placeholder DIV
    {
        if((http.readyState==4)&&(http.status==200)) {
            // locate the <DIV> we set up before that will contain the <SELECT> and set its contents
            document.getElementById('usercode_container').innerHTML=http.responseText;
        }
    }

    function LeftPad(el)
    {
        var padded = String('00000' + el.value).slice(-5);
        el.value = padded;
    }

</SCRIPT>