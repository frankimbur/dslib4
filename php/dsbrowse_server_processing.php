<?php
//  this script is called from DSBrowse:Display() when ajax calls are needed to do server side queries

// it is called from INSIDE THE CLASS, so that $this is set with the correct values of the DSBrowse instance
// is called after the browse script has loaded, so the code associated with the _browse.php script will be available
// for instatnce the functions set up with the $t->functions array


@session_start(); // this script is incompatible with php_compat54.inc, so it is skipped and we need to starts our session here

// check for a flag that can be sent to look for a previous search in the session. If so restore it
// look in dsbrowse_ssp_class.php, that is where it is saved
if ($_SESSION['storedsearch']) {
    $_REQUEST['search'] = $_GET['search'] = $_POST['search']=$_SESSION['storedsearch'];
    unset($_SESSION['storedsearch']);
}
else {
    $_SESSION['_search']=$_REQUEST['search']; //make the search 'sticky' in the session
}

// send the search values from our drop down into the datatable routine below through $_GET
if ($_REQUEST['searchby']) {
    $_REQUEST['search']['value'] = $_REQUEST['searchfor'];
    $_REQUEST['search']['column'] = $_REQUEST['searchby'];
}
else {
    $_REQUEST['search']['value'] = NULL;
    $_REQUEST['search']['column'] = NULL;
}
// DB table to use - set to the one in the DSBrowse class
$table = $this->classname;

// Table's primary key - also from the DSBrowse class
$primaryKey = $this->Key;
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

// convert the comma delimited list of columns from the DSBrowse class to the array required
if (! is_array($this->Columns)) {
    $dscols=explode(",",$this->Columns);
}
else {
    $dscols=$this->Columns;
}
$i=0;
unset($columns);
foreach ($dscols as $dscol) {
    // look for an alias in the column or use the column name as a default
    $dscol=ltrim(rtrim($dscol));
    $dscol=str_ireplace(" as "," AS ",$dscol);
    $pieces = explode(' AS ', $dscol);
    $column = $pieces[0];
    if (sizeof($pieces)>1) {
        $alias = array_pop($pieces);
    }
    else {
        $alias = $column;
    }
    // strip off trailing table names in aliases - contact.lastname becomes lastname
    $pieces = explode('.',$alias);
    if (sizeof($pieces)>1) {
        $alias=$pieces[1];
    }
    $columns[]=array('db' => $column,'dt'=> $i,'as'=>$alias);
    $i++;
}
// SQL server connection information
$sql_details = array(
    'user' => $GLOBALS['DSUSER'],
    'pass' => $GLOBALS['DSPASSWORD'],
    'db'   => $GLOBALS['DSDATABASE'],
    'host' => $GLOBALS['DSHOST']
);

require( 'dsbrowse_ssp_class.php' ); // standard server side processing script from datatables with a few modified lines to support the $functions array


$data = SSP::simple( $_REQUEST, $sql_details, $table, $primaryKey, $columns,$this->TableName ? "FROM ".$this->TableName : NULL,$this->WhereClause );
echo json_encode($data);