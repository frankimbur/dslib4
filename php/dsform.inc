<?php

    //******
    class DSForm extends form
    //******
    {
        var $AddAnother = 0;
        #If set (non zero), will include an 'add another' checkbox that
        #allows the user to carry data from one record to the next when
        #entering similar data.
        #Useful for situations where similar recurring data needs to be
        #entered. May not be used with NextPrev.

        var $NextPrev="";
        #If used, must be set to a field. Once set, the user will see a
        #Next and Prev button, both of  which will save the current data,
        #and move to the next (or previous) record in the table.
        #How does the routine know which is the next and previous order?
        #That's why you set this to a field.
        #In other words, if you want the user to move alphabetically
        #through a table by its 'name' column, set this member to 'name.

        var $FormName = "entryform";

        var $template = "dsedit.tpl"; //over-ride this to use a different smarty template

        var $skip_column_close = FALSE;

        function StartForm($Label, $action="") {
            global $PHP_SELF,$smarty;
            $this->add_element(array(
                "type"=>"submit",
                "name"=>"Submit",
                "onClick"=>"js()"
            ));

            if( $action == "" )
            {
                $action = $PHP_SELF;
            }

	        if ($_REQUEST['popup']) {
		        $this->template="dsedit_vanilla.tpl";
		        $this->add_element(array(
			        "type"=>"hidden",
			        "name"=>"popup",
			        "value"=>"{$_REQUEST['popup']}"
		        ));
	        }

            $smarty->assign("formname",$this->FormName);
            $smarty->assign("method",$this->FormMethod);
            $smarty->assign("action",$action);
            $smarty->assign("heading",$Label);
            $smarty->assign("DSHEADING",$Label);
            $smarty->assign("delete",$_REQUEST['delete']);
            $smarty->assign("key",$_REQUEST['key']);
        }


        function EndForm($adding=0,$jsafter="",$jsbefore="",$nosubmit=0)
        {
            global $smarty,$DSTEMPLATE;
            if (!$nosubmit) {
                $smarty->assign("submit",$this->show_element("Submit","Submit"));
            }
            //todo next prev
            $smarty->assign("elements",$this->elements);
            $DSTEMPLATE=$this->template;
        }

        function AddDate($Field,$CurrentRecord,$required=true)
        {
                $this->add_element(array("type"=>"text",
                    "name"=>$Field,
                    "date"=>TRUE,
                    "value"=>substr($CurrentRecord->$Field,0,10)));

        }
        function DrawDate($Name,$Label,$Required=0,$NoCalendar=0)
        {
            $this->DrawField($Name,$Label,$Required);
        }

        function AddTime($Field, $CurrentRecord)
        {
            $this->add_element(array("type"=>"text",
                "name"=>$Field."hour",
                "size"=>2,
                "maxlength"=>2,
                "value"=>substr($CurrentRecord->$Field,0,2),
                "valid_regex"=>"^[0-9]*$",
                "valid_e"=>"Invalid hour in $Field"));

            $this->add_element(array("type"=>"text",
                "name"=>$Field."minute",
                "size"=>2,
                "maxlength"=>2,
                "value"=>substr($CurrentRecord->$Field,3,2),
                "valid_regex"=>"^[0-9]*$",
                "valid_e"=>"Invalid minute in $Field"));

            if( substr($CurrentRecord->$Field,0,2) > 12 ) {
                $value = "PM";
            }
            else {
                $value = "AM";
            }

            $this->add_element(array("type"=>"select",
                "name"=>$Field."ampm",
                "options"=>array("AM","PM"),
                "value"=>$value ));

            $this->add_element(array("type"=>"hidden",
                "name"=>"field_".$Field,
                "value"=>$CurrentRecord->$Field));
        }
        function DrawTime($Name,$Label,$Required=0)
        {
            $this->DrawField($Name."hour",$Label,$Required,1);
            $this->show_element($Name."minute");
            $this->show_element($Name."ampm");

            echo "</td>";
        }

        function DrawField($Name,$Label,$Required=0,$SkipTableClose=0,$SkipLabel=0,$InsertAfterField="") {
	        if ( ! is_object( $this->elements[ $Name ]["ob"] ) ) {
		        die( "DSFORM:Drawfield fatal error: undefined element $Name" );
	        }
	        //store some internal elements
	        $this->elements[ $Name ]["ob"]->label             = $Label;
	        $this->elements[ $Name ]["ob"]->required          = $Required;
	        $this->elements[ $Name ]["ob"]->afterfield        = $InsertAfterField;
	        $this->elements[ $Name ]["ob"]->skip_column_close = $SkipTableClose;
	        $this->elements[ $Name ]["ob"]->afterfield        = $InsertAfterField;
	        // populate the html variable with the html that is correct for this element
	        if ( $this->elements[ $Name ]["ob"]->multiple && sizeof( $this->elements[ $Name ]["ob"]->options ) > 1 ) {
		        foreach ( $this->elements[ $Name ]["ob"]->options as $option ) {
			        $this->elements[ $Name ]["ob"]->html .= $this->show_element( $Name, $option['value'] );
			        $this->elements[ $Name ]["ob"]->html .= "<div class='form-control-inline'>{$option['label']}</div>";
		        }
	        } else {
		        $this->elements[ $Name ]["ob"]->html = $this->show_element( $Name );
	        }
            if ($Required) {
	            $star='<span class="required">*</span>';
            }
            else {
	            $star='';
            }

	        // begin actual output
	        if (!$SkipTableClose && !$SkipLabel) {
	            echo "
<div class=\"form-group\">
    <div class=\"col-sm-3\">
        <label class=\"control-label\">
            {$Label}{$star}
        </label>
    </div>
    <div class=\"col-sm-9\">
        {$this->elements[$Name]["ob"]->html}
    </div>
</div>";
            }
	        elseif ($SkipTableClose) {
		        echo "
<div class=\"form-group\">
    <div class=\"col-sm-3\">
        <label class=\"control-label\">
            {$Label}{$star}
        </label>
    </div>
    <div class=\"col - sm - 9\">
        {$this->elements[$Name]["ob"]->html}
    
";
	        }
	        elseif ($SkipLabel) {
		        echo "
    </div>
</div>";
            }
	        /*
	        echo '<label class="col-sm-2 control-label">';
	        if ( $Required ) {
		        echo '<span class="required">*</span>';
	        }
	        echo $Label . '</label>';
	        if ( ! $SkipLabel ) {
		        echo '<div class="col-sm-10">';
	        }
	        echo $this->elements[ $Name ]["ob"]->html;
	        if ( ! $SkipTableClose ) {
		        echo '</div>';
	        }
	        */
        }
        function DrawTextRow($text,$label="")
        {
            #echo "<label class='col-sm-2 control-label'>$label</label><div class='col-sm-10 control-label' style='min-height:30px;padding-top:0px;'>$text</div>";
	        echo "
<div class=\"form-group\">
    <div class=\"col-sm-3\">
        <label class=\"control-label\">
            {$label}
        </label>
    </div>
    <div class=\"col-sm-9 control-label\">
        {$text}
    </div>
</div>";


        }
        function FKLink($deleteorview,$table,$label,$returnhere="")
        {
            // ALL foo, not working yet...
            #return;
            if (!$deleteorview) {
                if (strlen($returnhere))
                    $url = $table."_edit.dtop";
                else
                    $url = $table."_edit.dtop";
                echo
                "
                <A HREF=$url target=_blank>$label</A>
                ";
            }
            echo "</td>";
        }


        #############
        # Function FileSelect
        # creates the link to the file select window
        # TJM 05/17/01
        # Params:
        # $field - the fieldname to receive the data
        # $dir="" - directory to retrieve, if blank, the fileselect script will get the cwd
        # $linktext="..." - text for the link
        # $linktype=1 - 1 for button, 0 for text
        # $excludedirs=1 - 1: excludedirs from listing,0: include them
        # $deleteorview=0 - if this is 1, don't do anything
        # $fileselectorpath - path and filename of the script that does the file selection
        #############
        Function FileSelect($field,$dir="",$linktext="...",$linktype=1,$excludedirs=1,$deleteorview=0,$fileselectorpath="php/fileselector.html")
        {
            if (!$dir) {
                $dir = getcwd();
            }
            if (!$linktext) {
                $linktext="...";
            }
            if (!$deleteorview) {
                if ($linktype==0) { //text link
                    #                echo "<a href=# onclick='ifield = document.forms[1].$field; chooser = window.open(\"$fileselectorpath?dir=".urlencode($dir)."&excludedirs=$excludedirs\", \"chooser\", \"toolbar=no,menubar=no,scrollbar=no,width=400,height   =300\"); chooser.ifield = ifield'>$linktext</a>";
                    echo "<a href=# onclick='ifield = document.".$this->FormName.".$field; chooser = window.open(\"$fileselectorpath?dir=".urlencode($dir)."&excludedirs=$excludedirs\", \"chooser\", \"toolbar=no,menubar=no,scrollbar=no,width=400,height=300\"); chooser.ifield = ifield'>$linktext</a>";

                }
                else {  //button (default)

                    #                echo "<INPUT onclick='ifield = document.forms[1].$field; chooser = window.open(\"$fileselectorpath?dir=".urlencode($dir)."&excludedirs=$excludedirs\", \"chooser\", \"toolbar=no,menubar=no,scrollbar=no,width=400,height=300\"); chooser.ifield = ifield' type=\"button\" value=\"$linktext\">";
                    echo " <INPUT class='DSFORM_button' onclick='ifield = document.".$this->FormName.".$field; chooser = window.open(\"$fileselectorpath?dir=".urlencode($dir)."&excludedirs=$excludedirs\", \"chooser\", \"toolbar=no,menubar=no,scrollbar=no,width=400,height=300\"); chooser.ifield = ifield' type=\"button\" value=\"$linktext\">";

                }
            }
            #        echo "</td>";
        }

        #############
        # Function FKSelect
        # creates the link to the foreign key select window
        # TJM 05/19/01
        # Params:
        # $field - required; the fieldname to receive the data
        # $table= - required; directory to retrieve, if blank, the fileselect script will get the cwd
        # $fieldlist="*" - list of fields for query
        # $linktext="..." - text for the link
        # $linktype=1 - 1 for button, 0 for text
        # $whereclause="" - the where clause for the query
        # $deleteorview=0 - if this is 1, don't do anything
        # $fkselectorpath - path and filename of the script that does the file selection
        # $orderby=1 - how to order the fields
        # closecell=1 - if true close cell, if false don't
        #############
        Function FKSelect($field,$table="",$fieldlist="*",$linktext="...",$linktype=1,$whereclause="",$deleteorview=0,$fkselectorpath="php/fkselector.html",$orderby=1,$closecell=1)
        {
            if (!$linktext) {
                $linktext="...";
            }
            if (!$deleteorview) {
                if ($linktype==0) { //text link
                    #                echo "<a href=# onclick='ifield = document.forms[0].$field; chooser = window.open(\"$fkselectorpath?table=".urlencode($table)."&orderby=$orderby&whereclause=".urlencode($whereclause)."&fields=".urlencode($fieldlist)."\", \"chooser\", \"toolbar=no,menubar=no,scrollbar=no,width=400,height=300\"); chooser.ifield = ifield'>$linktext</a>";
                    echo "<a href=# onclick='ifield = document.entryform.$field; chooser = window.open(\"$fkselectorpath?table=".urlencode($table)."&orderby=$orderby&whereclause=".urlencode($whereclause)."&fields=".urlencode($fieldlist)."\", \"chooser\", \"toolbar=no,menubar=no,scrollbar=no,width=400,height=300\"); chooser.ifield = ifield'>$linktext</a>";
                }
                else {  //button (default)

                    #                echo "<INPUT onclick='ifield = document.forms[0].$field; chooser = window.open(\"$fkselectorpath?table=".urlencode($table)."&orderby=$orderby&whereclause=".urlencode($whereclause)."&fields=".urlencode($fieldlist)."\", \"chooser\", \"toolbar=no,menubar=no,scrollbar=no,width=400,height=300\"); chooser.ifield = ifield' type=\"button\" value=\"$linktext\">";
                    echo "<INPUT onclick='ifield = document.entryform.$field; chooser = window.open(\"$fkselectorpath?table=".urlencode($table)."&orderby=$orderby&whereclause=".urlencode($whereclause)."&fields=".urlencode($fieldlist)."\", \"chooser\", \"toolbar=no,menubar=no,scrollbar=no,width=400,height=300\"); chooser.ifield = ifield' type=\"button\" value=\"$linktext\">";

                }
            }
            if ($closecell) {
                echo "</td>";
            }
        }

        ##############
        Function DisplayPicEditLink($file,$fieldname,$upload_dir="",$upload_url="")
        ##############
        # displays a thumbnail of an image for edit screens next to upload button
        {
	        $elname = "field_".$fieldname;
        	if (! is_object($this->elements[$elname]["ob"]) ) {
		        die("DSFORM:DisplayPicEditLink fatal error: undefined element $elname");
	        }
	        global $DSUPLOADURL,$DSUPLOADDIR,$DSTHISPATH,$CurrentRecord;
            if (!strlen($upload_dir)) {
                $upload_dir = $DSUPLOADDIR;
            }
            if (!strlen($upload_url)) {
                $upload_url = $DSUPLOADURL;
            }
	        $this->elements[$elname]["ob"]->displaypiceditlink=$fieldname;
	        $this->elements[$elname]["ob"]->upload_dir=$upload_dir;
	        $this->elements[$elname]["ob"]->upload_url=$upload_url;

	        if (strlen($file) && file_exists("$upload_dir/$file") ) {
	        	echo("<a class=popup href=\"$upload_url/$file\"><img class='form-control-inline ds_file_thumb' src=\"$upload_url/$file\"></a><span class='df_file_delete_label'>delete</span><input class='form-control-inline df_file_delete_checkbox' type=checkbox name=delete_$fieldname value=on>");
	        }
	        echo "</div></div>";
        }

        Function SaveAndReturn($table="",$key="",$keyval="",$option="")
            # returns back to the browser if it has been saved in the session
        {
        	# popup support:
	        # if this is form is supposed to update the select option on the calling screen it needs all the params sent
	        # example SaveAndReturn("artist","artist_no",36,"Adams, Smith");
            if ($_REQUEST['popup']) {
            	if (empty($table) || empty($key) || empty($keyval) || empty($option)) {
            		die("SaveAndReturn() error - popup specified but you did not pass table,key,keyval and option in SaveAndReturn call.");
	            }
            	$fname=$_REQUEST['popup'];
            	?>
	            <script>
                parent.$('#<?php echo $fname ?>').append('<option value="<?php echo $keyval ?>" selected="selected"><?php echo $option ?></option>');
            	parent.$.fn.colorbox.close();
	            </script>
				<?php
	            return;
            }

        	if  ($_SESSION['dsbrowse']){
                $dsbrowse=unserialize($_SESSION['dsbrowse']);
                //todo store and read the script name from this
                $_SESSION['storedsearch']=$_SESSION['_search'];
                Header("Location:  $dsbrowse->scriptname");
                exit;
            }
        }
    } //end DSForm

    

    #######
    Function SaveUploadedFile($varname,$fieldvalue,$deletefile,$directory="")
    #######
    # Saves files uploaded via browse button to $DSUPLOADDIR path
    # deletefile will delete the file and store blanks to the field
    {
        /*echo $varname;
        echo "--value".$fieldvalue;*/       
        global $HTTP_POST_FILES,$HTTP_POST_VARS,$DSUPLOADDIR;

        if (!strlen($directory)) {
            $directory = $DSUPLOADDIR;
        }
        #    else {
        ##        $dirname = "DS".strtoupper($directory)."DIR";
        #        global $$dirname;
        #        $directory = $$dirname;
        #    }

        # TJM don't upload, use selected file instead
        # the selected file is called fileselector_field_name_without_field
        # ex: fileselector_filename_image NOT fileselector_field_filename_image
        # this was done this way so it would work with the library's auto insert
        # and update mechanism
        $fileselectorname = "fileselector_".str_replace("field_","",$varname);
        if (@strlen($HTTP_POST_VARS["$fileselectorname"]) && $deletefile!="on") {
            # dont do an upload, the user chose an existing file from the fileselector
            $HTTP_POST_VARS[$varname]=$HTTP_POST_VARS["$fileselectorname"];
            return 1;
        }
        $destvarname=$varname."_name";
        $phpversion=floor(phpversion());
        if ($phpversion == 3) {
            $srcfile=$$varname;
            $destfile=$$destvarname;
        }
        else {
            $srcfile=$HTTP_POST_FILES[$varname]['tmp_name'];
            $destfile=$HTTP_POST_FILES[$varname]['name'];
        }
        if ((!strlen($srcfile) || $srcfile=="none") && $deletefile!='on') {
            $HTTP_POST_VARS[$varname] = $fieldvalue;
            return 1;
        }

        # FTI 9/30/07 remove apostrophes and commas from file name supplied by user
        $badchars=array(' ',',','"',"'",'|',"\\",'/','$','%','^','#','@','!','(',')','*','&','`','~',';',':','<','>',',');
        $destfile=str_replace($badchars,"",$destfile);
        # make spaces underscores
        $destfile=str_replace(' ',"",$destfile);
        $HTTP_POST_FILES[$varname]['name']=$HTTP_POST_VARS[$varname]=strtolower($destfile);
        # end of mod

        $destfilename=$directory."/".strtolower($destfile);
        $orgfile=$directory."/".strtolower($fieldvalue);

        if ($deletefile=="on") {
            $HTTP_POST_VARS["$varname"]="";

            if (file_exists($orgfile) && strlen($fieldvalue)) {
                unlink($orgfile);
            }

            return 1;
        }
        // Move file to a permanent location, checking for validity:
        if ($phpversion == 3) {
            if (!copy($srcfile,$destfilename))
                return 0;
            unlink($srcfile);
        }
        else {
            if (!move_uploaded_file($srcfile,$destfilename))
                return 0;
        }
        // Format file name for field item_filename...
        $HTTP_POST_VARS["$varname"]=basename($destfilename);
        return 1;
    }

    // Revised in order to keep unique name of file.
    #######
    Function SaveUploadedFileName($varname,$fieldvalue,$deletefile,$directory="")
    #######
    # Saves files uploaded via browse button to $DSUPLOADDIR path
    # deletefile will delete the file and store blanks to the field
    {
        /*echo $varname;
        echo "--value".$fieldvalue;*/       
        global $HTTP_POST_FILES,$HTTP_POST_VARS,$DSUPLOADDIR,$$destvarname,$$varname;

        if (!strlen($directory)) {
            $directory = $DSUPLOADDIR;
        }
        #    else {
        ##        $dirname = "DS".strtoupper($directory)."DIR";
        #        global $$dirname;
        #        $directory = $$dirname;
        #    }

        # TJM don't upload, use selected file instead
        # the selected file is called fileselector_field_name_without_field
        # ex: fileselector_filename_image NOT fileselector_field_filename_image
        # this was done this way so it would work with the library's auto insert
        # and update mechanism
        $fileselectorname = "fileselector_".str_replace("field_","",$varname);
        if (strlen($HTTP_POST_VARS["$fileselectorname"]) && $deletefile!="on") {
            # dont do an upload, the user chose an existing file from the fileselector
            $HTTP_POST_VARS[$varname]=$HTTP_POST_VARS["$fileselectorname"];
            return 1;
        }
        $destvarname=$varname."_name";
        $phpversion=floor(phpversion());
        if ($phpversion == 3) {
            $srcfile=$$varname;
            $destfile=$$destvarname;
        }
        else {
            $info = pathinfo($HTTP_POST_FILES[$varname]['name']);
            //pr($info);
            $fname = time().".".$info['extension'];
            $srcfile=$HTTP_POST_FILES[$varname]['tmp_name'];
            $destfile=$fname;
            //exit;
        }
        if ((!strlen($srcfile) || $srcfile=="none") && $deletefile!='on') {
            return 1;
        }

        # FTI 9/30/07 remove apostrophes and commas from file name supplied by user
        $badchars=array(' ',',','"',"'",'|',"\\",'/','$','%','^','#','@','!','(',')','*','&','`','~',';',':','<','>',',');
        $destfile=str_replace($badchars,"",$destfile);
        # make spaces underscores
        $destfile=str_replace(' ',"",$destfile);
        $HTTP_POST_FILES[$varname][name]=$HTTP_POST_VARS[$varname]=strtolower($destfile);
        # end of mod

        $destfilename=$directory."/".strtolower($destfile);
        $orgfile=$directory."/".strtolower($fieldvalue);

        if ($deletefile=="on") {
            $HTTP_POST_VARS["$varname"]="";

            if (file_exists($orgfile) && strlen($fieldvalue)) {
                unlink($orgfile);
            }

            return 1;
        }
        // Move file to a permanent location, checking for validity:
        if ($phpversion == 3) {
            if (!copy($srcfile,$destfilename))
                return 0;
            unlink($srcfile);
        }
        else {
            if (!move_uploaded_file($srcfile,$destfilename))
                return 0;
        }
        // Format file name for field item_filename...
        $HTTP_POST_VARS["$varname"]=basename($destfilename);
        return 1;
    }

    ##############
    Function DirArray($dir)
    ##############
    # Returns an array of files in a directory
    {
        $loops = 0;
        $d = dir("$dir");
        while($entry=$d->read()) {
            if (substr($entry,0,1)<>'.')
                $files[$loops++]=$entry;
        }
        $d->close();
        $files[$loops++]=' ';
        asort($files);
        return $files;
    }

    ##################
    class MultipleChoice
    ##################
    {
        var $key;
        var $table;
        var $tablekey;
        var $manytable;
        var $linktable;
        var $linkkey;
        var $linkcolumn;
        var $linkwc;
        var $size;
        var $title;
        function __construct()
        {
            $this->size=4;
        }
        function UpdateList($key)
        {
            global $HTTP_POST_VARS,$DSDEBUG;
            #$DSDEBUG=1;
            $this->key=$key;
            $selectedarray=$HTTP_POST_VARS[$this->manytable];
            DoQuery("DELETE from $this->manytable WHERE $this->tablekey=$this->key");
            if (!sizeof($selectedarray))
                return;
            while (list($field, $val) = each($selectedarray)) {
                $linkkey=OneSqlValue(
                    "SELECT $this->linkkey from
                    $this->linktable where $this->linkcolumn='$val'");
                DoQuery("INSERT INTO $this->manytable
                    ($this->linkkey,$this->tablekey) VALUES
                    ($linkkey,$this->key)");
            }
        }
        function DrawList()
        {
            global $DSWINDOWBG,$DSLABELFG;
            if (!$this->linkwc)
                $sql="SELECT $this->linkcolumn,$this->linkkey FROM $this->linktable ORDER BY 1";
            else
                $sql="SELECT $this->linkcolumn,$this->linkkey FROM $this->linktable WHERE $this->linkwc ORDER BY 1";                
            $result=DoQuery($sql);
            echo "
            <TR>
            <td bgcolor='$DSWINDOWBG' VALIGN=TOP><font color='$DSLABELFG'><B>$this->title:</B></FONT></TD>
            <TD bgcolor='$DSWINDOWBG' VALIGN=TOP><font color='$DSLABELFG'>
            <SELECT NAME=$this->manytable[] SIZE=$this->size MULTIPLE>";
            while ($Record = mysql_fetch_row($result)) {
                if (strlen($this->key) && strlen(OneSQLValue("
                    SELECT 1 FROM $this->manytable WHERE
                    $this->linkkey=$Record[1] AND
                    $this->tablekey=$this->key"))) {
                    $sel=" SELECTED ";
                }
                else {
                    $sel="";
                }
                echo "</OPTION>";
                echo "<OPTION value=\"$Record[1]\" $sel>$Record[0]";
            }
            echo "</SELECT>
            </td></tr>";
        }
    }
    /*  this class for wrote for multiple choice having no relationship  */
    ##################
    class MultipleSelect
    ##################
    {
        var $key;
        var $name;        
        var $table;        
        var $tablekey;
        var $columns;
        var $options;
        var $linkwc;
        var $size;
        var $title;
        var $hint;
        var $selectedval;        
        function __construct(){
            $this->size=6;
        }                
        function DrawList() {
            global $DSWINDOWBG,$DSLABELFG;            
            if(count($this->columns)>0){
                $fields = implode($this->columns,",");
            }            
            echo "
            <TR>
            <td bgcolor='$DSWINDOWBG' VALIGN=TOP><font color='$DSLABELFG'>$this->title:<br><B>$this->hint</B></FONT></TD>
            <TD bgcolor='$DSWINDOWBG' VALIGN=TOP><font color='$DSLABELFG'>
            <SELECT NAME=$this->name[] SIZE=$this->size MULTIPLE>";                       
            for($h=0;$h<count($this->options);$h++){
                if(isset($this->key)){
                    $sel = OneSQLValue("SELECT 1 FROM $this->table WHERE FIND_IN_SET('".$this->options[$h]['value']."',replace($this->selectedval, ' | ', ',')) AND $this->tablekey=$this->key");
                    if($sel){
                        $sel=" SELECTED ";
                    }
                    else{
                        $sel="";
                    }   
                }                
                echo "<OPTION $sel value=".$this->options[$h]['value']." $sel>".$this->options[$h]['label']."</OPTION>";
            }
            echo "</SELECT>
            </td></tr>";
        }
    }    
    #########################
    FUNCTION RestartForm()
    #########################
    ## PASS Vars by Reference!!
    ## this function can 'restart' a form with the current screen vars so that you can do validation
    ## its handy for validation that requires DB lookups
    ## gets inserted in an extra IF block right after the call to ReadCurrentRecord
    ## Example usage with standard library code:
    ## $CurrentRecord = ReadCurrentRecord("auth_user","auth_user_no",$key);
    ## if (strlen($Submit)) {
    ##      $isOk=OneSQLValue("Select Blah lah");
    ##        if ($isOK) {
    ##            JavascriptAlert("not ok!");
    ##            RestartForm(&$CurrentRecord,&$Submit,"auth_user");
    ##        }
    ##    }
    ##    else {
    ##        if (isset($do_over)) {
    ##    }    
    {
        global $Submit,$CurrentRecord,$form,$key,$HTTP_POST_VARS;
        // this unset causes the script to NOT save db values
        $Submit="";
        // this loop loads all the values from the POST (form) to the RAM variables so the form can re-draw
        // without the user losing any data.
        while (list($field, $val) = each($HTTP_POST_VARS)) {
            if ( substr($field,0,6)=="field_") {
                $fieldname=substr($field,6);
                $CurrentRecord->$fieldname=$val;
            }
        }    
        // this hidden field will cause the script to remember if it is supposed to add or edit a record
        if (strlen($key)) {
            $form->add_element(array(
                "type"=>"hidden",
                "name"=>"key",
                "value"=>$key
            ));
        }
    }
//***********
function FillSQLArray($query,$addblankrow=0)
    //***********
{
    $result = DoQuery($query);
    $loops=0;
    if ($addblankrow) {
        $retarray[$loops]["label"]="-- select one --";
        $retarray[$loops++]["value"]="";

    }
    if ($result) {
        while($row = mysqli_fetch_array($result)) {
            $retarray[$loops++]=$row[0];
        }
        mysqli_free_result($result);
        return $retarray;
    }
    return "";
}
//***********
function FillSQLArrayTwo($query,$addblankrow=0,$blankprompt="-- select one --")
    // Will fill a multi dimensional array with exactly two columns
    // formatted for use with OOForms select type
    //***********
{
    $result = DoQuery($query);
    $loops=0;
    if ($addblankrow){// && mysql_num_rows($result)>1) {
        $retarray[$loops]["label"]=$blankprompt;
        $retarray[$loops++]["value"]="";

    }
    if ($result) {
        while($row = mysqli_fetch_array($result)) {
            $retarray[$loops]["label"]=$row[0];
            $retarray[$loops++]["value"]=$row[1];
        }
        mysqli_free_result($result);
        return $retarray;
    }
    return Array("");
}
//***********
function YNSelectArray()
    //***********
    // Returns a string appropriate for a Yes/No select array
    // What the library uses for yes/no checkbox situations
{
    $yesno = array(array("label"=>"Yes","value"=>"Y"),
        array("label"=>"No","value"=>"N"));
    return $yesno;
}
//***********
function FormatHtmlDates($date,$HTTP_POST_VARS)
    //***********
    // backwards compatibility function it does nothing now that we have modern HTML date controls
{
    return $HTTP_POST_VARS[$date];
}
