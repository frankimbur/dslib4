<?php
/* OOHForms: textarea
 *
 * Copyright (c) 1998 by Jay Bloodworth
 *
 * $Id: of_textarea.inc,v 1.1.1.1 2001/11/28 20:11:54 tom Exp $
 */

class of_textarea extends of_element
{

    var $rows;
    var $cols;
    var $wrap;

    // Constructor
    function __construct($a)
    {
        $this->setup_element($a);
    }

    function self_get($val=NULL,$which=NULL, &$count=NULL)
    {
        $required = $this->minlength || $this->required ? "required" : "";

        $str = "";
        $str .= "<textarea $required class='$this->classname' name='$this->name' id='$this->name'";
        $str .= " rows='$this->rows' cols='$this->cols'";
        if ($this->wrap)
            $str .= " wrap='$this->wrap'";
        if ($this->extrahtml)
            $str .= " $this->extrahtml";
        $str .= ">" . htmlspecialchars($this->value) . "</textarea>";

        $count = 1;
        return $str;
    }

    function self_get_frozen($val, $which, &$count)
    {
        $str = "";
        $str .= "<textarea class='$this->classname' disabled name='$this->name' id='$this->name'";
        $str .= " rows='$this->rows' cols='$this->cols'";
        if ($this->wrap)
            $str .= " wrap='$this->wrap'";
        if ($this->extrahtml)
            $str .= " $this->extrahtml";
        $str .= ">" . htmlspecialchars($this->value) . "</textarea>";

        $count = 1;
        return $str;
    }

} // end TEXTAREA


