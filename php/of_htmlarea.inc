<?php
class of_htmlarea extends of_element {

  var $rows;
  var $cols;
  var $wrap;
  var $editor;
  var $width='100%';
  var $height=250;

  // Constructor
  function __construct($a) {
    $this->setup_element($a);
  }

  function self_get($val=NULL,$which=NULL, &$count=NULL) {
    $this->editor = new DSEditor($this->name);
    $this->editor->Value = $this->value;
    $str=$this->editor->CreateDSEditor($this->name,$this->width,$this->height) ;
    if ($this->extrahtml) 
      $str .= " $this->extrahtml";
    return $str;
  }
  function self_get_frozen($val,$which, &$count) {
    $str  = "";
    $str .= "<input type='hidden' name='$this->name'";
    $str .= " value='$this->value'>\n";
    $str .= "<table border=0><tr><td>\n";
    $str .=  nl2br($this->value);
    $str .= "\n</td></tr></table>\n";
   
    $count = 1;
    return $str;
  }
} // end HTMLAREA


