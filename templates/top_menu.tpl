{* top menu *}
<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a href='index.php'><img class="logo-image" src="img/1000px-banner.png"></a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a href="logout.php">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>

    </nav>
</div>
