<?php
class DSBrowse
{
    //****************************
    ### required variables ##################################

    var $classname;
    #        must be case sensitive name of the table - used to build _edit.dtop calls
    var $fields;
    #        column headings - may be less than specified in select (see hidefrom)

    #   (some are commented because they are declared in Table)
    var $Key;
    #        must be a column name, spelled just like in select statement
    var $Columns = "";
    # will be used in select statement can be a string or an array
    # As a string - "first,last,contact_no"
    # IMPORTANT
    # if you get syntax errors for complex queries that use expressions, formulas or aliases
    # send this as an array:
    # array("CONCAT(name,' ',last) AS fullname","address","city","substr(zip,1,10) AS ZIP")
    var $db;
    #        must be set to a database object - OBSOLETE

    ### optional variables ##################################
    var $WildCard = 1; //OBSOLETE
    var $TopHeading = "Data Listing";
    #        Appears on first line
    var $HideFrom = 0;
    #        Any columns beyond this # will not be selected, but not
    #        displayed - good for hiding system assigned keys
    var $ExtraUrl = "";
    #        if supplied, will be passed to edit routine. Can include vars
    var $NoEditing = 0;
    var $NoAdding = 0;
    #        flag to turn off add/edit/delete buttons - delete & edit go together
    var $ViewButton = 0;
    #        shows a different button in place of edit buttons, which must be turned off
    #        please see $ViewURL to set the URL of the button
    var $DisplayRows = 10;
    var $WhereClause = "";
    #        ie "salary > 0" - will be tacked on to every where clause built
    var $SearchChoices; //not supported yet
    #        the array that will display in drop down for searching
    #        i.e. "First Name"=>"first"
    var $TableName;
    #       if specified, can be a list of tables. Defaults to $class.
    var $ChangeOrder;
    #       a flag which will make the column headings clickable to adjust order
    var $LinkColumns = Array();
    #       an array which can hold keyed pairs of column names and  prefixes to
    #       display data as links. Example:
    #       $t->LinkColumns = array("domain"=>"http://","email"=>"mailto:");
    var $AddUrl;
    #       (optional) - if specified, the script url that will be linked to the
    #       add button. If not specified, this url will be built from class name
    #       plus '_add.dtop'.
    var $EditUrl;
    #       (optional) - if specified, the script url that will be linked to the
    #       edit button. If not specified, this url will be built from class name
    #       plus '_edit.dtop'.
    var $DeleteUrl;
    #       (optional) - if specified, the script url that will be linked to the
    #       delete button. If not specified, this url will be built from class name
    #       plus '_edit.dtop'.
    var $ViewURL;
    #       (optional) - (the edit buttons must be turned off) if specified, the script url that will be linked to the
    #       ViewButton. If not specified, this url will be built from class name
    #       plus '_view.dtop'.
    var $OrderBy;
    #       an optional orderby clause - do not include 'order by' defaults to '1'
    #       if the user changes the searchby field, this will be changed

    var $ScriptExtenson='.php';

    var $Functions;
    #       an array which can hold keyed pairs of column names and functions to
    #       call which will then be responsible for displaying that column.
    #       The function will be called with a single parameter, this browse object
    #       EXAMPLE:
    #       $t->Functions=Array("stats"=>"displaystats");
    #       ######
    #       Function displaystats($t)
    #       ######
    #       {
    #           $r=$t->record;
    #           if ($r[player_type]=='pitcher'))
    #               $data=$r[era];
    #           else
    #               $data=$r[battingaverage];
    #           if ($r[era]<2.00)
    #               $color='RED'
    #           else
    #               $color='BLACK';
    #           echo "<TD NOWRAP><FONT $t->Font COLOR $color>$data</FONT></TD>";
    #       }

    var $Template="dsbrowse.tpl";
    # This is the default smarty template. You can over-ride it and provide your own. If necessary once you do this you can also provide alternate versions of ds_browse_contents.pl and ds_close.tpl

    var $ServerScript = "php/dsbrowse_server_processing.php";
    # This is the default server side script that fetches data. You can over-ride it and provide your own. If necessary once you do this you can also provide alternate versions of ds_browse_ssp_class.php

    // obsolete variables, left here for legacy sake
    #var $DisplayNoResultsMessage = 0; //OBSOLETE
    #var $AbortIfNoRecords = 0; //OBSOLETE
    #var $TableCellSpacing = 0; //OBSOLETE
    #var $TableCellPadding = 0; //OBSOLETE
    #var $TableBorder = 1; //OBSOLETE
    #var $TableColor = ""; //OBSOLETE
    #var $HeadingBg = ""; //OBSOLETE
    #var $HeadingFg = ""; //OBSOLETE
    #var $ColDataFg = ""; //OBSOLETE
    #var $ColDataBg = ""; //OBSOLETE
    #var $css_prefix = ""; //OBSOLETE
    #var $Font = "Face='Arial' Size='2'"; //OBSOLETE

    ### used internally    ##################################

    var $scriptname;// will be set to $_SERVER['REQUEST_URI'];
    var $record; #array of current query record being processed

    function __construct() {
        $this->scriptname= $_SERVER['SCRIPT_NAME'].'?'.$_SERVER['QUERY_STRING'];
    }
    function display()
    {
        if (! empty(@$_REQUEST['draw']) ) { // this indicates we are being called by Ajax , so we pass control to the Ajax handler and exit. We do it this way so that all the code for the browse function exists before the Ajax handler executes.
            require($this->ServerScript);
            exit;
        }


        //todo support flexible last column
        global $smarty,$DSTEMPLATE;

        // a bunch of smarty variables get set here
        $smarty->assign('classname',$this->classname);
        $smarty->assign('hidefrom', $this->HideFrom ? $this->HideFrom : sizeof($this->fields));
        $smarty->assign('keycol',array_search($this->Key,$this->Columns));
        $smarty->assign("topheading",$this->TopHeading);
        $smarty->assign("fields",$this->fields);
        $smarty->assign("scriptname",$this->scriptname); //the ajax call always gets made contact_browse.php (not the server side script directly)
        $smarty->assign('displayrows', $this->DisplayRows);
        $smarty->assign('serverscript', $this->ServerScript);
        $smarty->assign('storedsearch', $_SESSION['storedsearch']['value']);


        // convert the order by format from the original one used by DSBrowse ("LASTNAME ASC or 1 DESC") to the format used by datatable (1=>'asc')
        // take care of the asc vs desc part
        if (stristr($this->OrderBy,"DESC")) {
            $smarty->assign("order", "desc");
        }
        else {
            $smarty->assign("order", "asc");
        }
        if ((int)$this->OrderBy ) { // a numeric was suppllied
            $this->OrderBy -= 1;
        }
        // look for an order by that looks lke 'LASTNAME' and convert it to a numeric
        $arr = explode(' ',trim($this->OrderBy));
        $orderbyfield=strtolower($arr[0]);
        $aColumns = @explode(',',trim($this->Columns));
        $aColumns=@array_map('strtolower', $aColumns);
        $orderbyNum=@array_search($orderbyfield,$aColumns);
        if ($orderbyNum) {
            $this->OrderBy=$orderbyNum;
        }
        $smarty->assign("orderby", (int) $this->OrderBy ? (int) $this->OrderBy : 0);

        //compose the ADD button,, which is not dynamic and is rendered in the smarty template
        if (!$this->NoAdding) {
            $addurl = strlen($this->AddUrl) ? $this->AddUrl : strtolower($this->classname)."_edit".$this->ScriptExtenson;

            if (strlen($this->ExtraUrl)) {
                if (! stristr($addurl)) {
                    $addurl .= "?";
                }
                $addurl .= $this->ExtraUrl;
            }
            $addbutton = "<a class='btn btn-primary' style='width:70px;' role='button' href='$addurl'>Add</a>";
        }
        else {
            $addbutton = "";
        }
        $smarty->assign("addbutton",$addbutton);


        //compose the EDIT button javascript, to get used in dsbrowse_close.tpl when we declare the DataTable JS object because it is dynamic
        if (!$this->NoEditing) {
            $editurl = strlen($this->EditUrl) ? $this->EditUrl : strtolower($this->classname)."_edit".$this->ScriptExtenson;
            $editbuttonjs = '<a class="btn btn-primary ds-edit-btn" role="button" href="'.$editurl.'?key=\'+data+\'">Edit</a>';
        }
        else {
            $editbuttonjs = "";
        }
        $smarty->assign("editbuttonjs",$editbuttonjs);

        //compose the DELETE button javascript, to get used in dsbrowse_close.tpl
        if (!$this->NoEditing) {
            $deletebuttonjs = '<a class="btn btn-primary ds-edit-btn" role="button" href="'.$editurl.'?delete=1&key=\'+data+\'">Delete</a>';
        }
        else {
            $deletebuttonjs = "";
        }
        $smarty->assign("deletebuttonjs",$deletebuttonjs);


        //compose the VIEW button javascript, to get used in dsbrowse_close.tpl
        if ($this->NoEditing && $this->ViewButton) {
            $viewurl = strlen($this->ViewURL) ? $this->ViewURL : strtolower($this->classname)."_edit".$this->ScriptExtenson;
            $viewbuttonjs = '<a class="btn btn-primary ds-edit-btn" role="button" href="'.$viewurl.'?key=\'+data+\'">View</a>';
        }
        else {
            $viewbuttonjs = "";
        }
        $smarty->assign("viewbuttonjs",$viewbuttonjs);

        //compose the search drop down
        $searchoptions="";
        if (count($this->SearchChoices)) {
            while ($slice=each($this->SearchChoices)) {
                $searchoptions.= "<option ";
                if ($slice[1]==$_REQUEST['searchby'])
                    $searchoptions .= " selected ";
                $searchoptions.= "value=$slice[1]>$slice[0]" ;
            }
            $smarty->assign("searchoptions",$searchoptions);
            $smarty->assign("searchfor",$_REQUEST['searchfor']);
        }
        else {
            $smarty->assign("searchoptions",NULL);
            $smarty->assign("searchfor",NULL);

        }


        // store this class in the session so that the server_processing.php script can find it later in an Ajax call where it will do the SQL stuff
        $_SESSION['dsbrowse'] = serialize($this);


        // set the template that will be be displayed in DsEndPage()
        $DSTEMPLATE=$this->Template;
    }
}
