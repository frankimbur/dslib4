<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{$heading}</h5>
            </div>
            <div class="ibox-content">
                <form method="post" enctype="multipart/form-data" class="form-horizontal">
                        {if $delete}
                            <div class="form-group">
                                    <div class="alert alert-danger">Press SUBMIT to delete this record...</div>
                            </div>
                        {/if}
                        {if $delete}
                            <fieldset disabled="disabled">
                        {/if}
                        {foreach from=$elements item=element}
                            {if $element['ob']->type=='of_hidden'}
                                <input type="hidden" name="{$element['ob']->name}" value="{$element['ob']->value}">
                            {/if}
                        {/foreach}
                        {$contents}
                    {if $delete}
                        </fieldset>
                    {/if}
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-primary" type="submit" name="Submit" value="Submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
